const ROUTES = {
  home: "/",
  faqs: "/faqs",
  about: "/about",
  privacy: "/privacy",
  terms: "/terms",
  new: "/new",
  jobs: "/jobs",
  job: "/jobs/:slug",
  companies: "/companies",
  company: "/companies/:slug",
  signUp: "/signup",
  signIn: "/signin",
  changePassword: "/changepassword",
  changePasswordConfirm: "/changepasswordconfirm",
  forgotPassword: "/forgotpassword",
  forgotPasswordVerification: "/forgotpasswordverification",
  welcome: "/welcome",
  thankyou: "/thankyou",
  account: "/account"
};

export default ROUTES
