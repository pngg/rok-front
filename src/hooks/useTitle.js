import React from "react";

import { TitleContext } from "../context/TitleContext";

const useTitle = () => {
  const [title, setTitle] = React.useContext(TitleContext);

  return title;
};

export default useTitle;
