import React from "react";

import { StripePropsContext } from "../context/StripePropsContext";

const useStripeProps = () => {
  const [stripeProps] = React.useContext(StripePropsContext);

  return stripeProps;
};

export default useStripeProps;
