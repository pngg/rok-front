import React from "react";

import { JobContext } from "../context/JobContext";

const useJob = () => {
  const [jobData] = React.useContext(JobContext);

  return jobData;
};

export default useJob;
