import React from "react";

import { PriceContext } from "../context/PriceContext";

const usePrice = () => {
  const [totalPrice] = React.useContext(PriceContext);

  return totalPrice;
};

export default usePrice;
