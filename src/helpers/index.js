import { differenceInDays, format, formatDistance, parseISO } from "date-fns";
import DOMPurify from "dompurify";

const formatCurrency = amount => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 0
  });

  return formatter.format(amount);
};

const capitalizeWord = word => {
  return word.charAt(0).toUpperCase() + word.substr(1);
};

const formatYYYYMMDD = date => {
  if (!date) {
    return null;
  }
  return format(toISO(date), "yyy-MM-dd");
};

const formatMonthDD = date => {
  return format(toISO(date), "MMMM d");
};

const distanceInWordsFromNow = date => {
  return formatDistance(toISO(date), Date.now(), {
    addSuffix: true,
    includeSeconds: true
  });
};

const totalNights = (from, to) => {
  return differenceInDays(toISO(to), toISO(from));
};

const toISO = date => {
  if (typeof date === "string") {
    return parseISO(date);
  }
  return date;
};

const purifyHtml = dirty => {
  const clean = DOMPurify.sanitize(dirty);
  return { __html: clean };
};

const stripePubKey = "pk_test_aflGmmONGvNblC2Yihqyvi4k";

const selectCustomStyles = {
  control: (provided, state) => ({
    ...provided,
    padding: 4,
    color: "#4a5568",
    lineHeight: "1.25",
    backgroundColor: state.isFocused ? "white" : "#edf2f7",
    borderColor: state.isFocused ? "#a0aec0" : "#edf2f7",
    borderWidth: "1px",
    borderRadius: ".25rem",
    boxShadow: state.isFocused ? 0 : 0,
    "&:hover": {
      borderWidth: "1px"
    }
  }),
  option: (provided, state) => ({
    ...provided,
    backgroundColor: state.isFocused ? "#edf2f7" : "white",
    color: state.isSelected ? "#B44D12" : "#0A558C"
  }),
  indicatorSeparator: (provided, state) => ({
    ...provided,
    display: state.isFocused ? "block" : "none"
  }),
  menuPortal: (provided, state) => ({ ...provided, zIndex: 9999 })
};

const validateJobDescription = (field, form) => {
  if (!form.values.jobDescription) {
    form.setFieldError(field.name, "Job Description can't be blank");
    form.setFieldTouched(field.name, true, false);
  }
};

const validateCompanyDescription = (field, form) => {
  if (!form.values.jobDescription) {
    form.setFieldError(field.name, "Company Description can't be blank");
    form.setFieldTouched(field.name, true, false);
  }
};

export {
  formatCurrency,
  capitalizeWord,
  formatYYYYMMDD,
  formatMonthDD,
  distanceInWordsFromNow,
  totalNights,
  purifyHtml,
  stripePubKey,
  selectCustomStyles,
  validateJobDescription,
  validateCompanyDescription
};
