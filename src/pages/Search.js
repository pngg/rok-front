import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import ClipLoader from "react-spinners/ClipLoader";

import SearchCriteria from "../components/search/SearchCriteria";
// import { SEARCH_JOBS_QUERY } from '../queries';
import { GET_ALL_JOBS_QUERY } from "../graphql/queries";
import JobList from "../components/Job/view/JobList";

const Search = () => {
  const [criteria, setCriteria] = useState({});
  const { loading, data, fetchMore } = useQuery(GET_ALL_JOBS_QUERY, {
    variables: {
      filter: criteria,
      limit: 10,
      offset: 0
    }
  });

  const handleCriteriaChange = criteria => {
    setCriteria({ criteria: criteria });
  };

  console.log({criteria});

  const handleScroll = ({ currentTarget }, data, fetchMore) => {
    if (
      currentTarget.scrollTop + currentTarget.clientHeight >=
      currentTarget.scrollHeight
    ) {
      handleFetchMore(data, fetchMore);
    }
  };

  const handleFetchMore = (data, fetchMore) => {
    fetchMore({
      variables: {
        offset: data.jobs.length
      },
      updateQuery: (prevResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prevResult;
        return Object.assign({}, prevResult, {
          jobs: [...prevResult.jobs, ...fetchMoreResult.jobs]
        });
      }
    });
  };

  return (
    <>
      <div className="container mx-auto">
        <div className="">
          <SearchCriteria criteria={criteria} onChange={handleCriteriaChange} />
        </div>
      </div>
      <div className="flex flex-wrap items-center">
        {data && data.jobs && (
          <div className="w-full">
            <h5>{data.jobs.length} Jobs Await...</h5>
            <JobList jobs={data.jobs} />
          </div>
        )}
      </div>
      <div className="container mx-auto py-6">
        <div className="flex justify-center items-center">
          <button
            className="button"
            onClick={e => handleScroll(e, data, fetchMore)}
          >
            Load More
          </button>
        </div>
      </div>
    </>
  );
};

export default Search;
