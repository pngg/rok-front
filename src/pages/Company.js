import React from 'react'
import { useQuery } from "@apollo/react-hooks";
import { Link } from "react-router-dom";

import NoData from "../utils/NoData";

import { GET_JOB_QUERY } from "../graphql/queries";

const Company = props => {
  const { data, loading } = useQuery(GET_JOB_QUERY, {
    variables: { id: parseInt(props.match.params.slug) },
    fetchPolicy: "network-only"
  });

  return (
    <>
      {data && data.job && (
        <div className="container mx-auto bg-white flex flex-wrap px-8 py-8"></div>
      )}

      {!loading && !data && <NoData />}
    </>
  );
}

export default Company
