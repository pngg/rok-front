import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { NavLink } from "react-router-dom";
import Transition from "../utils/Transition";

import logo from "../assets/images/remote-ok-logo.svg";
import bg from "../assets/images/dnomad.svg";
import hero from "../assets/images/hero.svg";
import notFound from "../assets/images/not-found.svg";

// Queries
import { GET_ALL_JOBS_QUERY } from "../graphql/queries";

import SearchCriteria from "../components/search/SearchCriteria";
import JobList from "../components/Job/view/JobList";
import SubscribeForm from "../utils/Subscribe";
import ClipLoader from "react-spinners/ClipLoader";

const Home = () => {
  const [criteria, setCriteria] = React.useState({});
  const [open, setOpen] = React.useState(false);
  const { loading, data, fetchMore } = useQuery(GET_ALL_JOBS_QUERY, {
    variables: {
      filter: criteria,
      limit: 10,
      offset: 0,
    },
  });

  const handleCriteriaChange = (criteria) => {
    setCriteria({ criteria: criteria });
  };

  console.log({ criteria });

  // Destructure 'event' to get currentTarget
  const handleScroll = ({ currentTarget }, data, fetchMore) => {
    if (
      currentTarget.scrollTop + currentTarget.clientHeight >=
      currentTarget.scrollHeight
    ) {
      handleFetchMore(data, fetchMore);
    }
  };

  const handleFetchMore = (data, fetchMore) => {
    fetchMore({
      variables: {
        offset: data.jobs.length,
      },
      updateQuery: (prevResult, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prevResult;
        return Object.assign({}, prevResult, {
          jobs: [...prevResult.jobs, ...fetchMoreResult.jobs],
        });
      },
    });
  };

  return (
    <>
      <div className="relative bg-gray-50 overflow-hidden">
        <div className="hidden sm:block sm:absolute sm:inset-y-0 sm:h-full sm:w-full">
          <div className="relative h-full max-w-full mx-auto">
            <img className="object-cover" src={hero}></img>
          </div>
        </div>

        <div className="relative pt-6 pb-12 sm:pb-16 md:pb-20 lg:pb-28 xl:pb-32">
          <div className="max-w-screen-xl mx-auto px-4 sm:px-6">
            <nav className="relative flex items-center justify-between sm:h-10 md:justify-center">
              <div className="flex items-center flex-1 md:absolute md:inset-y-0 md:left-0">
                <div className="flex items-center justify-between w-full md:w-auto">
                  <a href="/">
                    <img
                      className="h-8 w-auto sm:h-10"
                      src={logo}
                      alt="Remote OK"
                    />
                  </a>
                  <div className="-mr-2 flex items-center md:hidden">
                    <button
                      onClick={() => setOpen(true)}
                      type="button"
                      className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                    >
                      <svg
                        className="h-6 w-6"
                        stroke="currentColor"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M4 6h16M4 12h16M4 18h16"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
              <div className="hidden md:block">
                <a
                  href="/jobs"
                  className="font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out"
                >
                  All jobs
                </a>
                <a
                  href="/new"
                  className="ml-10 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out"
                >
                  Post a job
                </a>
                <a
                  href="/about"
                  className="ml-10 font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition duration-150 ease-in-out"
                >
                  About
                </a>
              </div>
              <div className="hidden md:absolute md:flex md:items-center md:justify-end md:inset-y-0 md:right-0">
                <span className="inline-flex rounded-md shadow">
                  <a
                    href="/signin"
                    className="inline-flex items-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-indigo-600 bg-white hover:text-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-gray-50 active:text-indigo-700 transition duration-150 ease-in-out"
                  >
                    Log in
                  </a>
                </span>
              </div>
            </nav>
          </div>

          {open && (
            <div className="absolute top-0 inset-x-0 p-2 md:hidden">
              <Transition
                show={open}
                enter="duration-150 ease-out"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="duration-100 ease-in"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <div className="rounded-lg shadow-md transition transform origin-top-right">
                  <div className="rounded-lg bg-white shadow-xs overflow-hidden">
                    <div className="px-5 pt-4 flex items-center justify-between">
                      <div>
                        <img
                          className="h-8 w-auto"
                          src={logo}
                          alt="Remote OK"
                        />
                      </div>
                      <div className="-mr-2">
                        <button
                          onClick={() => setOpen(false)}
                          type="button"
                          className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
                        >
                          <svg
                            className="h-6 w-6"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 24 24"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M6 18L18 6M6 6l12 12"
                            />
                          </svg>
                        </button>
                      </div>
                    </div>
                    <div className="px-2 pt-2 pb-3">
                      <a
                        href="/"
                        className="block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                      >
                        Product
                      </a>
                      <a
                        href="/"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                      >
                        Features
                      </a>
                      <a
                        href="/"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                      >
                        Marketplace
                      </a>
                      <a
                        href="/"
                        className="mt-1 block px-3 py-2 rounded-md text-base font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50 focus:outline-none focus:text-gray-900 focus:bg-gray-50 transition duration-150 ease-in-out"
                      >
                        Company
                      </a>
                    </div>
                    <div>
                      <a
                        href="/"
                        className="block w-full px-5 py-3 text-center font-medium text-indigo-600 bg-gray-50 hover:bg-gray-100 hover:text-indigo-700 focus:outline-none focus:bg-gray-100 focus:text-indigo-700 transition duration-150 ease-in-out"
                      >
                        Log in
                      </a>
                    </div>
                  </div>
                </div>
              </Transition>
            </div>
          )}

          <div className="mt-10 mx-auto max-w-screen-xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 xl:mt-28">
            <div className="text-center">
              <h2 className="text-4xl tracking-tight leading-10 font-extrabold text-primary-900 sm:text-5xl sm:leading-none md:text-6xl">
                Organize your lifestyle <br className="xl:hidden" />
                and start working remotely
              </h2>
              <p className="mt-3 max-w-md mx-auto text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl md:max-w-3xl">
                Remote OK is the easiest way to find remote jobs, careers and
                other remote work opportunities at interesting and innovative
                companies.
              </p>
              <div className="mt-5 max-w-md mx-auto sm:flex sm:justify-center md:mt-8">
                <div className="rounded-md shadow">
                  <a
                    href="/"
                    className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:shadow-outline-indigo transition duration-150 ease-in-out md:py-4 md:text-lg md:px-10"
                  >
                    Post a job
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {loading ? (
        <div className="text-center my-15">
          <ClipLoader css={{}} size={75} color={"silver"} loading={true} />
        </div>
      ) : data && data.jobs ? (
        <>
          <div className="container mx-auto px-4 sm:px-6 lg:px-8">
            <h1 className="text-2xl font-extrabold mt-6 mb-3">Today</h1>
            {/* filter today's jobs */}
            <JobList jobs={data.jobs} />
          </div>

          <div className="container mx-auto px-4 sm:px-6 lg:px-8">
            <h1 className="text-2xl font-extrabold mt-6 mb-3">Yesterday</h1>
            <JobList jobs={data.jobs} />
          </div>

          <div className="container mx-auto px-4 sm:px-6 lg:px-8">
            <h1 className="text-2xl font-extrabold mt-6 mb-3">Last 7 days</h1>
            <JobList jobs={data.jobs} />
          </div>

          <div className="container mx-auto px-4 sm:px-6 lg:px-8">
            <h1 className="text-2xl font-extrabold mt-6 mb-3">This month</h1>
            <JobList jobs={data.jobs} />
          </div>
        </>
      ) : (
        <div className="relative max-w-xl mx-auto px-4 sm:px-6 lg:px-8 lg:max-w-screen-xl my-20">
          <p className="mt-4 max-w-3xl mx-auto text-center text-xl sm:text-3xl leading-7 text-gray-500">
            Sorry, no jobs found!
          </p>
          {/* <img className="object-center h-96" src={notFound}></img> */}
        </div>
      )}

      <div className="relative max-w-xl mx-auto px-4 sm:px-6 lg:px-8 lg:max-w-screen-xl">
        <div className="relative">
          <h3 className="text-center text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-5xl sm:leading-10">
            Actively searching?
          </h3>
          <p className="mt-4 max-w-2xl mx-auto text-center text-xl leading-7 text-gray-500">
            Join our weekly Remote Jobs newsletter and be the first one to know
            about new remote opportunities.
          </p>
        </div>
      </div>
      <div className="mt-5 mb-20 relative max-w-3xl mx-auto px-4 sm:px-6 lg:px-8 lg:max-w-screen-xl">
        <SubscribeForm />
      </div>
    </>
  );
};

export default Home;
