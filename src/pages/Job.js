import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { NavLink } from "react-router-dom";
import { format } from "date-fns";
import { MdArrowBack } from "react-icons/md";
import {
  TwitterShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  WorkplaceShareButton,
  EmailShareButton,
  TwitterIcon,
  FacebookIcon,
  LinkedinIcon,
  TelegramIcon,
  WhatsappIcon,
  WorkplaceIcon,
  EmailIcon
} from "react-share";
import ClipLoader from "react-spinners/ClipLoader";

import { formatCurrency, capitalizeWord, purifyHtml } from "../helpers";
import NoData from "../utils/NoData";

import { GET_JOB_QUERY } from "../graphql/queries";

const Job = props => {
  const { data, loading } = useQuery(GET_JOB_QUERY, {
    variables: { id: parseInt(props.match.params.slug) },
    fetchPolicy: "network-only"
  });

  const shareUrl = `https://www.remote-ok.com/${props.match.params.slug}`;

  return (
    <>
      {loading && <ClipLoader />}
      {data && data.job && (
        <section className="bg-white mt-32">
          <div className="container mx-auto mt-40 pl-8">
            <div className="flex">
              <MdArrowBack />
              <NavLink to="/" className="label">
                Back to all jobs
              </NavLink>
            </div>
          </div>
          <div className="container mx-auto bg-white flex flex-wrap p-8 pt-10">
            <div className="flex flex-wrap md:w-3/4 pr-10">
              <div className="w-full">
                <span className="label">
                  Posted {format(new Date(data.job.publishedAt), "MMM dd")}
                </span>
                <h1 className="text-5xl font-bold">{data.job.jobTitle}</h1>
                <div className="flex flex-wrap items-center py-2 mb-5 mt-2">
                  {data.job.jobType && (
                    <span className="help text-yellow-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      {capitalizeWord(data.job.jobType)}
                    </span>
                  )}
                  {data.job.minSalary && (
                    <span className="help text-green-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      {data.job.currency}
                      {formatCurrency(data.job.minSalary)}
                      {" - "}
                      {formatCurrency(data.job.maxSalary)} {"/ yr"}
                    </span>
                  )}
                  {data.job.offersEquity && (
                    <span className="help text-green-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Equity
                    </span>
                  )}
                  {data.job.visaSponsor && (
                    <span className="help text-red-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Visa sponsor
                    </span>
                  )}
                  {data.job.paidRelocation && (
                    <span className="help text-blue-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Paid relocation
                    </span>
                  )}
                  {data.job.regions &&
                    data.job.regions.map(region => (
                      <span
                        key={region.id}
                        className="help text-blue-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2"
                      >
                        Remote {region.regionName}
                      </span>
                    ))}
                </div>
              </div>
              <div className="w-full">
                <p
                  className=""
                  dangerouslySetInnerHTML={purifyHtml(
                    `${data.job.jobDescription}`
                  )}
                />
              </div>
              <div className="flex flex-wrap sm:justify-between justify-center sm:items-center w-full mt-12">
                <div className="flex sm:w-2/3">
                  <a
                    href="{data.job.applyUrl}"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="rounded text-white font-bold bg-primary-500 hover:bg-primary-700 hover:shadow-lg p-3 hover:no-underline cursor-pointer"
                  >
                    Apply for this position
                  </a>
                </div>
                <div className="flex flex-col sm:w-1/3 sm:mt-0 mt-10 justify-end text-center">
                  <h3 className="label">Share this job</h3>
                  <div className="flex justify-center a2a_kit a2a_kit_size_32 a2a_default_style">
                    <TwitterShareButton
                      url={shareUrl}
                      quote={"data.job.jobTitle"}
                    >
                      <TwitterIcon size={32} round />
                    </TwitterShareButton>
                    <FacebookShareButton
                      url={shareUrl}
                      quote={data.job.jobTitle}
                    >
                      <FacebookIcon size={32} round />
                    </FacebookShareButton>
                    <LinkedinShareButton
                      url={shareUrl}
                      quote={data.job.jobTitle}
                    >
                      <LinkedinIcon size={32} round />
                    </LinkedinShareButton>
                    <WorkplaceShareButton
                      url={shareUrl}
                      quote={data.job.jobTitle}
                    >
                      <WorkplaceIcon size={32} round />
                    </WorkplaceShareButton>
                    <EmailShareButton url={shareUrl} quote={data.job.jobTitle}>
                      <EmailIcon size={32} round />
                    </EmailShareButton>
                  </div>
                </div>
              </div>
            </div>

            <div className="md:w-1/4 w-full">
              <div className="flex flex-col justify-center items-center bg-neutral-100 max-w-full rounded overflow-hidden shadow-lg">
                <div className="flex flex-col justify-center items-center w-full text-center p-6 border-b">
                  <div className="pb-2/3">
                    <img
                      className="h-full w-full object-cover rounded-full"
                      src={data.job.companyLogo}
                      alt={data.job.companyName}
                    />
                  </div>
                  <p className="pt-2 text-lg font-semibold">
                    {data.job.companyName}
                  </p>
                  <a
                    className="text-sm text-gray-600 hover:no-underline cursor-pointer hover:text-primary-0"
                    href={data.job.companyUrl}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    View website
                  </a>
                  <div className="mt-5">
                    <a
                      href={data.job.applyLink}
                      className="border rounded-full py-2 px-4 text-xs font-semibold text-gray-700 hover:no-underline cursor-pointer"
                    >
                      Apply for this position
                    </a>
                  </div>
                </div>
                <div className="flex justify-center w-full px-6 py-4 text-center">
                  <a
                    className="inline-block rounded-full px-3 py-1 text-xs font-semibold text-gray-600 mr-2 hover:no-underline cursor-pointer hover:text-primary-0"
                    href={`/companies/${data.job.company.slug}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    View more jobs
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
      {!loading && !data && <NoData />}
    </>
  );
};

export default Job;
