import React from "react";

const Faqs = () => {
  return (
    <>
      <section className="bg-white my-24">
        <div className="container mx-auto px-8">
          <div className="flex items-center">
            <div className="flex">
              <p className="text-base">
                faqs
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Faqs;
