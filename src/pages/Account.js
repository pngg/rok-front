import React from "react";
import { Route, Link } from "react-router-dom";

import RouteGuard from "../components/Auth/RouteGuard";
import SignOut from "../components/Auth/SignOut";
import Dashboard from "../components/Profile/Dashboard";
import JobPostList from "../components/Profile/JobList";
import OrderList from "../components/Order/OrderList";
import EditProfile from "../components/Profile/EditProfile";

import { TitleContext } from "../context/TitleContext";

const Account = ({ match }) => {
  const [title, setTitle] = React.useContext(TitleContext);

  return (
    <RouteGuard>
      <section className="mt-32">
        <div className="container mx-auto max-w-5xl flex justify-between items-start pt-20">
          <div className="flex flex-col w-1/5 shadow-md bg-gray-200 py-8 mr-5">
            <nav className="flex flex-col p-4 sm:pl-8">
              <p className="label">My Account</p>
              <Link
                to={`${match.url}`}
                className="py-1 text-primary-700 hover:text-primary-0 hover:no-underline"
              >
                Profile
              </Link>
              <Link
                to={`${match.url}/jobs`}
                className="py-1 text-primary-700 hover:text-primary-0 hover:no-underline"
              >
                Job Posts
              </Link>
              <Link
                to={`${match.url}/orders`}
                className="py-1 text-primary-700 hover:text-primary-0 hover:no-underline"
              >
                Orders
              </Link>
              <Link
                to={`${match.url}/settings`}
                className="pt-1 text-primary-700 hover:text-primary-0 hover:no-underline"
              >
                Settings
              </Link>
            </nav>
          </div>
          <div className="flex flex-col justify-center shadow-md w-4/5 px-8 pb-8">
            <div className="flex justify-center w-full mb-10">
              <h1 className="text-3xl uppercase">{title}</h1>
            </div>
            <div className="w-full">
              <Route exact path={`${match.path}`} component={Dashboard} />
              <Route path={`${match.path}/jobs`} component={JobPostList} />
              <Route path={`${match.path}/orders`} component={OrderList} />
              <Route path={`${match.path}/settings`} component={EditProfile} />
            </div>
          </div>
        </div>
      </section>
    </RouteGuard>
  );
};

export default Account;
