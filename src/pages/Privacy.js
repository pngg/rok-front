import React from 'react'

const Privacy = () => {
  return (
    <>
      <section className="bg-white my-24">
        <div className="container mx-auto px-8">
          <div className="flex items-center">
            <div className="flex">
              <p className="text-base">
                privacy
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Privacy;
