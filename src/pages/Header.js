import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import { FaSignInAlt } from "react-icons/fa";

import { UserContext } from "../context/UserContext";
import SignOut from "../components/Auth/SignOut";

import logo from "../assets/images/remote-ok-logo.svg";

const Header = () => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);
  console.log({currentUser});

  return (
    <>
      <header className="absolute inset-x-0 top-0 flex flex-wrap items-center w-full px-2 pt-12">
        <div className="container mx-auto flex justify-between items-center">
          <NavLink to="/" className="flex items-center">
            <svg
              className="block w-12 h-12"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 850.394 850.394"
            >
              <path
                fill="#043D5C"
                d="M413.999 57.738c-85.978 0-164.985 29.597-227.577 79.048L86.569 57.738l172.73 253.544.232 274.117h88.8v-82.853l32.198-34.691 168.208-185.618 15.187-17.253H458.627L348.332 394.979V264.984l-102.715-81.338c47.723-33.333 105.743-52.974 168.382-52.974 162.667 0 294.506 131.876 294.506 294.524 0 52.527-13.81 101.842-37.934 144.53l55.157 49.965c35.283-56.418 55.748-123.055 55.748-194.495 0-202.947-164.529-367.458-367.477-367.458z"
              />
              <path
                fill="#1A1A1A"
                d="M466.943 424.798l-50.208 55.43 188.713 168.625c-51.499 44.124-118.329 70.858-191.449 70.858-162.657 0-294.514-131.858-294.514-294.515 0-67.161 22.523-129.024 60.358-178.563l-42.794-62.804C80.717 248.4 46.53 332.796 46.53 425.197c0 202.948 164.52 367.459 367.468 367.459 97.68 0 186.384-38.195 252.166-100.377l137.699 37.669-336.92-305.15z"
              />
            </svg>
          </NavLink>
          <nav className="flex items-center justify-between">
            <NavLink
              to="/new"
              className="border border-primary-0 rounded hover:shadow hover:no-underline p-2 mr-5 uppercase"
            >
              Post a job
            </NavLink>
            {currentUser && (
              <div className="relative group">
                <div className="flex items-center cursor-pointer text-sm text-blue border border-white border-b-0  group-hover:border-grey-light rounded-t-lg py-1 px-2">
                  <img
                    src={
                      currentUser.attributes.picture ||
                      `https://api.adorable.io/avatars/285/${currentUser.attributes.email}.png`
                    }
                    className="h-10 w-10 rounded-full mr-2"
                    alt={
                      currentUser.attributes.name && currentUser.attributes.name
                    }
                  />
                  {(currentUser.attributes.name &&
                    currentUser.attributes.name) ||
                    currentUser.attributes.email}
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
                </div>
                <div className="items-center absolute border border-t-0 rounded-b-lg p-1 bg-white p-2 invisible group-hover:visible w-full">
                  <NavLink
                    to="/account"
                    className="px-4 py-2 block text-black hover:bg-grey-lighter"
                  >
                    View Profile
                  </NavLink>
                  <NavLink
                    to="/account/settings"
                    className="px-4 py-2 block text-black hover:bg-grey-lighter"
                  >
                    Edit Profile
                  </NavLink>
                  <hr className="border-t mx-2 border-grey-light" />
                  <div className="flex items-center px-4 py-2 block text-black hover:bg-grey-lighter">
                    <SignOut />
                  </div>
                </div>
              </div>
            )}
            {!currentUser && (
              <div className="flex justify-between items-center">
                <NavLink
                  to="/signin"
                  className="text-primary-700 hover:text-primary-0 hover:no-underline mr-1"
                >
                  Signin
                </NavLink>
                <span className="text-primary-700 hover:text-primary-0">
                  <FaSignInAlt />
                </span>
              </div>
            )}
            {currentUser && (
              <NavLink to="/account" className="">
                <img
                  src={currentUser.picture}
                  alt=""
                  className="h-full w-full object-cover rounded-full"
                />
              </NavLink>
            )}
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
