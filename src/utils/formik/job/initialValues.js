const initialValues = {
  jobTitle: "",
  jobType: "full-time",
  jobLocation: "",
  regions: [],
  tags: "",
  jobDescription: "",
  applyLink: "",
  minSalary: 0,
  maxSalary: 0,
  offersEquity: false,
  visaSponsor: false,
  paidRelocation: false,
  companyName: "",
  companyMission: "",
  companyLogo: {},
  companyUrl: "",
  companyTwitter: "",
  companyVideo: "",
  companyDescription: "",
  highlightJob: false,
  displayLogo: false,
  cardHolderName: ""
};

export default initialValues;
