import * as Yup from "yup";

const FILE_SIZE = 5000000;
const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/gif", "image/png"];

const JobSchema = Yup.object().shape({
  jobTitle: Yup.string().required("Job Title can't be blank"),
  jobType: Yup.string().required("Job Type can't be blank"),
  jobLocation: Yup.string(),
  regions: Yup.array()
    .of(
      Yup.object().shape({
        label: Yup.string(),
        value: Yup.string()
      })
    )
    .nullable()
    .required("Region can't be blank"),
  jobDescription: Yup.string(),
  // jobDescription: Yup.string().required("Job Description can't be blank"),
  companyName: Yup.string().required("Company Name can't be blank"),
  companyLogo: Yup.mixed()
    .required("Logo can't be blank")
    // .test(
    //   "required",
    //   "Logo can't be blank",
    //   value =>
    //     value == null ||
    //     (Object.entries(value).length < 0 && value.constructor === Object)
    // )
    .test(
      "fileSize",
      "File too large",
      value => value && value.size <= FILE_SIZE
    )
    .test(
      "fileFormat",
      "Unsupported Format",
      value => value && SUPPORTED_FORMATS.includes(value.type)
    ),
  companyMission: Yup.string(),
  companyUrl: Yup.string()
    .url("Company URL must be a valid URL")
    .required("Website Url can't be blank"),
  companyTwitter: Yup.string(),
  companyVideo: Yup.string().url("Company Video URL must be a valid URL"),
  companyDescription: Yup.string(),
  applyLink: Yup.string().required("Apply Link can't be blank"),
  cardHolderName: Yup.string().required("Name can't be blank")
});

export default JobSchema;
