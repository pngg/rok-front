import React from "react";

const NoData = () => (
  <h2 className="text-2xl text-center mt-32 pt-20">Sorry, we didn't find anything!</h2>
);

export default NoData;
