const eeClient = require("elasticemail-webapiclient").client;

const options = {
  apiKey: process.env.REACT_APP_ELASTICEMAIL_API_KEY,
  apiUri: "https://api.elasticemail.com/",
  apiVersion: "v2",
};

const EE = new eeClient(options);

export default EE;
