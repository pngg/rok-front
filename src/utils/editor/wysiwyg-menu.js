/** @jsx jsx */

import { jsx } from "@emotion/core";
import { bubblePositioner, useRemirrorContext } from "@remirror/react";
import { useRemirrorTheme } from "@remirror/ui";
import { GoHorizontalRule } from "react-icons/go";
import {
  BoldIcon,
  CodeIcon,
  H1Icon,
  H2Icon,
  H3Icon,
  ItalicIcon,
  LinkIcon,
  ListOlIcon,
  ListUlIcon,
  QuoteRightIcon,
  RedoAltIcon,
  RulerHorizontalIcon,
  StrikethroughIcon,
  TimesIcon,
  UnderlineIcon,
  UndoAltIcon
} from "@remirror/ui-icons";
import { useEffect, useRef, useState } from "react";
import keyNames from "w3c-keyname";

import {
  BubbleContent,
  BubbleMenuTooltip,
  IconButton,
  Toolbar
} from "./wysiwyg-components";

const menuItems = [
  ["bold", [BoldIcon]],
  ["italic", [ItalicIcon]],
  ["underline", [UnderlineIcon]],
  ["strike", [StrikethroughIcon]],
  ["toggleHeading", [H1Icon, "1"], { level: 1 }],
  ["toggleHeading", [H2Icon, "2"], { level: 2 }],
  ["toggleHeading", [H3Icon, "3"], { level: 3 }],
  ["undo", [UndoAltIcon]],
  ["redo", [RedoAltIcon]],
  ["toggleBulletList", [ListUlIcon]],
  ["toggleOrderedList", [ListOlIcon]],
  ["blockquote", [QuoteRightIcon]],
  ["toggleCodeBlock", [CodeIcon]],
  ["horizontalRule", [GoHorizontalRule]]
];

const runAction = (method, attrs) => e => {
  e.preventDefault();
  method(attrs);
};

/**
 * Retrieve the state for the button
 */
const getButtonState = (active, inverse = false) =>
  active
    ? inverse
      ? "active-inverse"
      : "active-default"
    : inverse
    ? "inverse"
    : "default";

/**
 * The MenuBar component which renders the actions that can be taken on the text within the editor.
 */
export const MenuBar = ({ inverse, activateLink }) => {
  const { actions } = useRemirrorContext();

  return (
    <Toolbar>
      {menuItems.map(([name, [Icon, subText], attrs], index) => {
        const buttonState = getButtonState(
          actions[name].isActive(attrs),
          inverse
        );

        return (
          <MenuItem
            index={index}
            key={index}
            Icon={Icon}
            subText={subText}
            state={buttonState}
            disabled={!actions[name].isEnabled()}
            onClick={runAction(actions[name], attrs)}
          />
        );
      })}
      <MenuItem
        Icon={LinkIcon}
        state={getButtonState(actions.updateLink.isActive(), inverse)}
        disabled={!actions.updateLink.isEnabled()}
        onClick={activateLink}
      />
    </Toolbar>
  );
};

/**
 * A single clickable menu item for editing the styling and format of the text.
 */
const MenuItem = ({
  state,
  onClick,
  Icon,
  variant,
  disabled = false,
  index
}) => {
  return (
    <IconButton
      onClick={onClick}
      state={state}
      disabled={disabled}
      index={index}
    >
      <Icon variant={variant} />
    </IconButton>
  );
};

export const BubbleMenu = ({
  linkActivated = false,
  deactivateLink,
  activateLink
}) => {
  const { actions, getPositionerProps, helpers } = useRemirrorContext();

  const positionerProps = getPositionerProps({
    ...bubblePositioner,
    hasChanged: () => true,
    isActive: params => {
      const answer =
        (bubblePositioner.isActive(params) || linkActivated) &&
        !actions.toggleCodeBlock.isActive() &&
        !helpers.isDragging();
      return answer;
    },
    positionerId: "bubbleMenu"
  });

  const { bottom, ref, left } = positionerProps;

  const updateLink = href => actions.updateLink({ href });
  const removeLink = () => actions.removeLink();
  const canRemove = () => actions.removeLink.isActive();

  return (
    <BubbleMenuTooltip ref={ref} bottom={bottom + 5} left={left}>
      {linkActivated && (
        <LinkInput {...{ deactivateLink, updateLink, removeLink, canRemove }} />
      )}
    </BubbleMenuTooltip>
  );
};

const LinkInput = ({ deactivateLink, updateLink, removeLink, canRemove }) => {
  const [href, setHref] = useState("");
  const { css } = useRemirrorTheme();
  const wrapperRef = useRef(null);

  const onChange = event => {
    setHref(event.target.value);
  };

  const submitLink = () => {
    updateLink(href);
    deactivateLink();
  };

  const onKeyPress = event => {
    const key = keyNames.keyName(event.nativeEvent);
    if (key === "Escape") {
      event.preventDefault();
      deactivateLink();
    }

    if (key === "Enter") {
      event.preventDefault();
      submitLink();
    }
  };

  const onClickRemoveLink = event => {
    event.preventDefault();
    removeLink();
    deactivateLink();
  };

  const handleClick = event => {
    if (!wrapperRef.current || wrapperRef.current.contains(event.target)) {
      return;
    }
    deactivateLink();
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick, false);
    return () => {
      document.removeEventListener("mousedown", handleClick, false);
    };
  });

  return (
    <BubbleContent ref={wrapperRef}>
      <input
        placeholder="Enter URL..."
        autoFocus={true}
        onChange={onChange}
        // onBlur={deactivateLink}
        onSubmit={submitLink}
        onKeyPress={onKeyPress}
        css={css`
          border: none;
          color: #003e6b;
          background-color: transparent;
          min-width: 150px;
          padding: 0 10px;
        `}
      />
      {canRemove() && (
        <MenuItem
          Icon={TimesIcon}
          state="active-inverse"
          onClick={onClickRemoveLink}
          variant="inverse"
        />
      )}
    </BubbleContent>
  );
};
