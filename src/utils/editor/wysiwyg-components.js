/** @jsx jsx */

import { jsx } from '@emotion/core';
import { useRemirrorTheme } from '@remirror/ui';
import { ResetButton } from '@remirror/ui-buttons';
import { forwardRef } from 'react';

export const Menu = forwardRef((props, ref) => {
  const { sx } = useRemirrorTheme();

  return (
    <div
      {...props}
      ref={ref}
      css={sx({
        '& > button': {
          display: 'inline-block',
        },
      })}
    />
  );
});

Menu.displayName = 'Menu';

export const Toolbar = props => {
  const { sx } = useRemirrorTheme();

  return (
    <Menu
      {...props}
      css={sx({
        position: "relative",
        padding: "7px 0",
        border: "1px solid #a0aec0",
        borderRadius: ".25rem",
        marginBottom: "5px"
      })}
    />
  );
};

export const IconButton = forwardRef((props, ref) => {
  const { sx } = useRemirrorTheme();

  return (
    <ResetButton
      {...props}
      type="button"
      ref={ref}
      css={sx(
        {
          marginLeft: 3
        },
        props.css
      )}
    />
  );
});

IconButton.displayName = 'IconButton';

/**
 * Allows positioners to work.
 */
export const EditorWrapper = forwardRef((props, ref) => {
  const { sx } = useRemirrorTheme();

  return <div {...props} ref={ref} css={sx({ position: 'relative'})} />;
});

EditorWrapper.displayName = 'EditorWrapper';

export const BubbleMenuTooltip = forwardRef((props, ref) => {
  const { css } = useRemirrorTheme();

  return (
    <span
      {...props}
      ref={ref}
      css={css`
        z-index: 10;
        position: absolute;
        bottom: ${props.bottom}px;
        left: ${props.left}px;
        padding-bottom: 9px;
        transform: translateX(-50%);
      `}
    />
  );
});

BubbleMenuTooltip.displayName = 'BubbleMenuTooltip';

export const BubbleContent = forwardRef((props, ref) => {
  const { css } = useRemirrorTheme();

  return (
    <span
      {...props}
      ref={ref}
      css={css`
        background: #edf2f7;
        border-radius: 0.25rem;
        font-size: 0.75rem;
        line-height: 1.4;
        padding: 0.75em;
        text-align: center;
        display: flex;
      `}
    />
  );
});

BubbleContent.displayName = 'BubbleContent';
