import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

const Subscribe = () => {
  const initialValues = {
    activationReturnUrl: "",
    activationTemplate: "",
    alreadyActiveUrl: "",
    apiKey: process.env.REACT_APP_ELASTICEMAIL_API_KEY,
    consentDate: "",
    consentIP: "",
    email: "",
    firstName: "",
    notifyEmail: "",
    publicAccountID: "4d27ea58-3d60-4d54-b6ab-e4d4976bcfdb",
    publicListID: "f230ede6-89f1-44c3-bbbe-b361e1efa8e8",
    returnUrl: "",
    source: "WebForm",
    sourceUrl: "",
    verifyEmail: true,
  };
  const UserSchema = Yup.object().shape({
    name: Yup.string(),
    email: Yup.string()
      .email("Email must be a valid Email")
      .required("Email can't be blank"),
  });

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    const formData = new FormData();
    Object.entries(values).map(([key, value]) => formData.append(key, value));
    for (var data of formData) {
      console.log(data);
    }
    try {
      console.log(values);
      const elasticAddContactEndpoint =
        "https://api.elasticemail.com/v2/contact/quickadd";
      const requestOptions = {
        method: "POST",
        headers: {
          // "Content-Type": "multipart/form-data",
          "Content-Type": "application/json",
        },
        // body: formData,
        body: JSON.stringify(values),
      };
      fetch(elasticAddContactEndpoint, requestOptions)
        .then((response) => response.json())
        .then((data) => console.log(data));
    } catch (error) {
      console.log(error.message);
      setErrors({ EEError: error.message });
      setSubmitting(false);
    }
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserSchema}
        onSubmit={handleSubmit}
      >
        {({ values, touched, dirty, errors, isValid, isSubmitting }) => {
          return (
            <Form className="mt-5 max-w-full mx-auto sm:flex sm:justify-center md:mt-8">
              <div className="mr-0 sm:mr-3 relative rounded-md shadow-sm">
                <Field
                  name="firstName"
                  type="text"
                  placeholder="Enter your name"
                  className="mt-3 sm:mt-0 appearance-none block w-full px-3 py-3 border border-gray-300 text-base leading-6 rounded-md placeholder-gray-500 shadow-sm focus:outline-none focus:placeholder-gray-400 focus:shadow-outline focus:border-blue-300 transition duration-150 ease-in-out sm:flex-1"
                />
              </div>
              <div className="relative rounded-md shadow-sm">
                <Field
                  name="email"
                  type="text"
                  placeholder="Enter your email"
                  className={`${
                    errors.email && touched.email
                      ? "border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red"
                      : "focus:border-blue-300"
                  } mt-3 sm:mt-0 appearance-none block w-full px-3 py-3 border border-gray-300 text-base leading-6 rounded-md placeholder-gray-500 shadow-sm focus:outline-none focus:placeholder-gray-400 focus:shadow-outline focus:border-blue-300 transition duration-150 ease-in-out sm:flex-1`}
                />
                {errors.email && touched.email ? (
                  <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                    <svg
                      className="h-5 w-5 text-red-500"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                    >
                      <path
                        fillRule="evenodd"
                        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                ) : null}
              </div>
              <button
                type="submit"
                className="mt-3 w-full px-6 py-3 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-gray-800 shadow-sm hover:bg-gray-700 focus:outline-none focus:shadow-outline active:bg-gray-900 transition duration-150 ease-in-out sm:mt-0 sm:ml-3 sm:flex-shrink-0 sm:inline-flex sm:items-center sm:w-auto"
                disabled={isSubmitting}
              >
                Sign me up!
              </button>
              {errors.EEError && <div className="error">{errors.EEError}</div>}
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default Subscribe;
