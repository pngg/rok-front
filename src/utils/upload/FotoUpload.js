import React from "react";

import { ThumbContext } from "../../context/ThumbContext";

const FotoUpload = ({ field, form }) => {
  const [thumb, setThumb] = React.useContext(ThumbContext);

  console.log({ formErrors: form.errors });

  const handleImageChange = e => {
    e.preventDefault();
    let file = e.target.files[0];
    if (file) {
      try {
        const reader = new FileReader();
        reader.onload = () => {
          const dataURL = reader.result;
          setThumb({ dataURL, file });
        };
        reader.readAsDataURL(file);
        form.setFieldValue(field.name, file);
        // form.setFieldTouched(field.name, true);
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <input
      name={field.name}
      type="file"
      accept="image/*"
      onChange={handleImageChange}
    />
  );
};

export default FotoUpload;
