import React from "react";

import { ThumbContext } from "../../context/ThumbContext";

const Thumb = ({ file }) => {
  const [thumb, setThumb] = React.useContext(ThumbContext);
  
  if (!file) {
    return null;
  }
  //  If you want to access file contents you have to use the FileReader API:
  const reader = new FileReader();

  reader.onload = () => {
    // Do whatever you want with the file contents
    const dataURL = reader.result;
    setThumb(dataURL);
  };

  reader.readAsDataURL(file);

  return (
    <img
      src={thumb}
      alt={file.name}
      className="object-scale-down h-24 object-center rounded-full mx-auto"
    />
  );
};

export default Thumb;
