export const uploadSingleImageCloudinary = async file => {
  try {
    const formData = new FormData();

    formData.append("file", file);
    formData.append("upload_preset", "kurator");

    const res = await fetch(
      "https://api.cloudinary.com/v1_1/pnggcodes/image/upload",
      {
        method: "POST",
        body: formData
      }
    );
    const cloudinaryObject = await res.json();
    return cloudinaryObject.secure_url;
  } catch (err) {
    // Do nothing and return default field value which is a string coz GraphQL expects a string result
    return;
  }
};
