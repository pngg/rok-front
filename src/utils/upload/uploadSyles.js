export const baseStyle = {
  flex: 1,
  display: "flex",
  cursor: "pointer",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 1,
  borderRadius: ".25rem",
  borderColor: "#a0aec0",
  borderStyle: "dashed",
  backgroundColor: "#edf2f7",
  color: "#102A43",
  outline: "none",
  transition: "border .24s ease-in-out"
};

export const activeStyle = {
  borderColor: "#2196f3"
};

export const acceptStyle = {
  borderColor: "#00e676"
};

export const rejectStyle = {
  borderColor: "#ff1744"
};
