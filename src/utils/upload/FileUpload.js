import React from "react";
import { useDropzone } from "react-dropzone";
import { ErrorMessage } from "formik";

import { ThumbContext } from "../../context/ThumbContext";
import {
  baseStyle,
  activeStyle,
  acceptStyle,
  rejectStyle
} from "./uploadSyles";

const FileUpload = ({
  field: { name },
  form: { setFieldValue, setFieldTouched, errors, touched }
}) => {
  const [thumb, setThumb] = React.useContext(ThumbContext);

  const onDrop = React.useCallback(async acceptedFiles => {
    const file = acceptedFiles[0];
    try {
      const reader = new FileReader();
      reader.onload = () => {
        const dataURL = reader.result;
        setThumb({ dataURL, file });
      };
      reader.readAsDataURL(file);
      setFieldValue(name, file);
      setFieldTouched(name, true);
    } catch (err) {
      console.log(err);
    }
  }, []);

  console.log({ errors });

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({
    onDrop,
    accept: "image/*",
    multiple: false
  });

  const style = React.useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {})
    }),
    [isDragActive, isDragReject]
  );

  return (
    <>
      <div
        {...getRootProps({ style, onBlur: () => setFieldTouched(name, true) })}
      >
        <input {...getInputProps()} />
        {thumb ? (
          <img
            src={thumb.dataURL}
            alt={thumb.file.name}
            className="object-scale-down h-24 object-center rounded-full mx-auto"
          />
        ) : (
          <p className="help">
            Drag 'n' drop logo here, or click to select file
          </p>
        )}
      </div>
      {/* {touched.companyLogo && errors.companyLogo && (
        <ErrorMessage name="companyLogo" component="div" className="error" />
      )} */}
    </>
  );
};

export default FileUpload;
