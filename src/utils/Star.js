import React from "react";

const Star = () => (
  <span className="star-rating color-orange">
    <i className="fa fa-star" />
  </span>
);

export default Star;
