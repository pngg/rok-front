import React from "react";
import LoadingOverlay from "react-loading-overlay";
import ClipLoader from "react-spinners/ClipLoader";

const StripeOverlay = props => {
  return (
    <>
      {props.stripe ? (
        <LoadingOverlay
          active={!props.isSubmitting}
          spinner={<ClipLoader size={150} color={"#003E6B"} />}
          text="Processing your payment..."
          styles={{
            wrapper: base => ({
              ...base,
              position: "relative"
            }),
            overlay: base => ({
              ...base,
              color: "#003E6B",
              background: "rgba(255, 255, 255, 1)"
            }),
            content: base => ({
              ...base,
              alignItems: "center",
              flexDirection: "column",
              display: "flex"
            })
          }}
        >
          {props.children}
        </LoadingOverlay>
      ) : (
        props.children
      )}
    </>
  );
};

export default StripeOverlay;
