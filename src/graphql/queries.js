import gql from "graphql-tag";
import { JOB_FIELDS_FRAGMENT } from "../graphql/fragments";

// USER QUERIES
const ME_QUERY = gql`
  query me {
    me {
      id
      name
      email
      role
      jobs {
        ...jobFields
      }
      errors {
        key
        message
      }
    }
  }

  ${JOB_FIELDS_FRAGMENT}
`;

const GET_ALL_USERS_QUERY = gql`
  query users {
    users {
      id
      name
      email
      role
      jobs {
        ...jobFields
      }
    }
  }

  ${JOB_FIELDS_FRAGMENT}
`;

// JOB QUERIES
const GET_JOB_QUERY = gql`
  query job($id: ID!) {
    job(id: $id) {
      ...jobFields
    }
  }

  ${JOB_FIELDS_FRAGMENT}
`;

const GET_ALL_JOBS_QUERY = gql`
  query jobs($limit: Int!, $offset: Int!) {
    jobs(limit: $limit, offset: $offset) {
      ...jobFields
    }
  }

  ${JOB_FIELDS_FRAGMENT}
`;

const SEARCH_JOBS_QUERY = gql`
  query searchJobs($filter: JobFilter!) {
    jobs(filter: $filter) {
      ...jobFields
    }
  }

  ${JOB_FIELDS_FRAGMENT}
`;

const GET_CURRENCIES_QUERY = gql`
  query currencies {
    currencies{
      id
      currency_name
      currency_code
      currency_symbol
    }
  }
`;

export {
  ME_QUERY as default,
  GET_ALL_USERS_QUERY,
  GET_JOB_QUERY,
  GET_ALL_JOBS_QUERY,
  SEARCH_JOBS_QUERY,
  GET_CURRENCIES_QUERY
};
