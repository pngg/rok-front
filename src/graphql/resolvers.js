import gql from "graphql-tag";

const UPDATE_CART_MUTATION = gql`
  mutation {
    updateCart @client
  }
`;

export { UPDATE_CART_MUTATION as default };
