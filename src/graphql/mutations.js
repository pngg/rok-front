import gql from "graphql-tag";
import USER_FIELDS_FRAGMENT, {
  CHARGE_FIELDS_FRAGMENT,
  JOB_FIELDS_FRAGMENT
} from "../graphql/fragments";

// USER MUTATIONS
const SIGNUP_USER_MUTATION = gql`
  mutation signup($name: String, $email: String!, $password: String!) {
    signup(input: { name: $name, email: $email, password: $password }) {
      token
      user {
        ...userFields
      }
    }
  }
  ${USER_FIELDS_FRAGMENT}
`;

const SIGNIN_USER_MUTATION = gql`
  mutation signin($email: String!, $password: String!) {
    signin(input: { email: $email, password: $password }) {
      token
      user {
        ...userFields
      }
    }
  }
  ${USER_FIELDS_FRAGMENT}
`;

const UPDATE_USER_MUTATION = gql`
  mutation updateUser($name: String, $email: String!, $password: String!) {
    updateUser(input: { name: $name, email: $email, password: $password }) {
      ...userFields
    }
  }
  ${USER_FIELDS_FRAGMENT}
`;

const DELETE_USER_MUTATION = gql`
  mutation deleteUser {
    deleteUser {
      ...userFields
    }
  }
  ${USER_FIELDS_FRAGMENT}
`;

// JOB MUTATIONS
const CREATE_JOB_DRAFT_MUTATION = gql`
  mutation createJobDraft(
    $jobTitle: String!
    $jobType: String!
    $jobLocation: String
    $regions: [String]
    $tags: [String]
    $jobDescription: String!
    $applyLink: String!
    $minSalary: Decimal
    $maxSalary: Decimal
    $offersEquity: Boolean
    $visaSponsor: Boolean
    $paidRelocation: Boolean
    $highlightJob: Boolean
    $displayLogo: Boolean
    # $company: CompanyParams
    $companyName: String!
    $companyMission: String
    $companyLogo: Upload!
    $companyUrl: String!
    $companyTwitter: String
    $companyVideo: String
    $companyDescription: String
  ) {
    createJobDraft(
      input: {
        jobTitle: $jobTitle
        jobType: $jobType
        jobLocation: $jobLocation
        regions: $regions
        tags: $tags
        jobDescription: $jobDescription
        applyLink: $applyLink
        minSalary: $minSalary
        maxSalary: $maxSalary
        offersEquity: $offersEquity
        visaSponsor: $visaSponsor
        paidRelocation: $paidRelocation
        highlightJob: $highlightJob
        displayLogo: $displayLogo
        # company: $company
        companyName: $companyName
        companyMission: $companyMission
        companyLogo: $companyLogo
        companyUrl: $companyUrl
        companyTwitter: $companyTwitter
        companyVideo: $companyVideo
        companyDescription: $companyDescription
      }
    ) {
      ...jobFields
    }
  }
  ${JOB_FIELDS_FRAGMENT}
`;

const CREATE_JOB_MUTATION = gql`
  mutation createJob(
    $amount: Int!
    $currency: String!
    $source: String!
    $description: String
    $jobTitle: String!
    $jobType: String!
    $jobLocation: String
    $regions: [String]
    $tags: [String]
    $jobDescription: String!
    $applyLink: String!
    $minSalary: Decimal
    $maxSalary: Decimal
    $offersEquity: Boolean
    $visaSponsor: Boolean
    $paidRelocation: Boolean
    $highlightJob: Boolean
    $displayLogo: Boolean
    $companyName: String!
    $companyMission: String
    $companyLogo: Upload!
    $companyUrl: String!
    $companyTwitter: String
    $companyVideo: String
    $companyDescription: String
  ) {
    createJob(
      stripeInput: {
        amount: $amount
        currency: $currency
        source: $source
        description: $description
      }
      jobInput: {
        jobTitle: $jobTitle
        jobType: $jobType
        jobLocation: $jobLocation
        regions: $regions
        tags: $tags
        jobDescription: $jobDescription
        applyLink: $applyLink
        minSalary: $minSalary
        maxSalary: $maxSalary
        offersEquity: $offersEquity
        visaSponsor: $visaSponsor
        paidRelocation: $paidRelocation
        highlightJob: $highlightJob
        displayLogo: $displayLogo
        companyName: $companyName
        companyMission: $companyMission
        companyLogo: $companyLogo
        companyUrl: $companyUrl
        companyTwitter: $companyTwitter
        companyVideo: $companyVideo
        companyDescription: $companyDescription
      }
    ) {
      charge {
        ...chargeFields
      }
      job {
        ...jobFields
      }
    }
  }
  ${CHARGE_FIELDS_FRAGMENT}
  ${JOB_FIELDS_FRAGMENT}
`;

const PUBLISH_JOB_MUTATION = gql`
  mutation publishJob(
    $amount: Int!
    $currency: String!
    $source: String!
    $description: String
    $jobId: ID!
  ) {
    publishJob(
      stripeInput: {
        amount: $amount
        currency: $currency
        source: $source
        description: $description
      }
      jobId: $jobId
    ) {
      charge {
        ...chargeFields
      }
      job {
        ...jobFields
      }
    }
  }
  ${CHARGE_FIELDS_FRAGMENT}
  ${JOB_FIELDS_FRAGMENT}
`;

// COMMUNICATION MUTATIONS
const SUBSCRIBE_EMAIL_MUTATION = gql`
  mutation subscribe($email: String!, $interval: String!) {
    subscribe(input: { email: $email, interval: $interval }) {
      email
      interval
    }
  }
`;

export {
  SIGNUP_USER_MUTATION as default,
  SIGNIN_USER_MUTATION,
  UPDATE_USER_MUTATION,
  DELETE_USER_MUTATION,
  SUBSCRIBE_EMAIL_MUTATION,
  CREATE_JOB_DRAFT_MUTATION,
  CREATE_JOB_MUTATION,
  PUBLISH_JOB_MUTATION
};
