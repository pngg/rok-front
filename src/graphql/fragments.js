import gql from "graphql-tag";


const COMPANY_FIELDS_FRAGMENT = gql`
  fragment companyFields on Company {
    id
    slug
    companyName
    companyUrl
    companyLogo
    companyMission
    companyTwitter
    companyVideo
    companyDescription
    jobs {
      id
    }
    errors {
      key
      message
    }
  }
`;

const CHARGE_FIELDS_FRAGMENT = gql`
  fragment chargeFields on Charge {
    id
    amount
    currency
    description
    receiptUrl
    balanceTransaction
    paymentMethod
    source {
      brand
      name
      country
      expMonth
      expYear
      last4
    }
    created
  }
`;

const JOB_FIELDS_FRAGMENT = gql`
  fragment jobFields on Job {
    id
    slug
    jobTitle
    jobType
    jobLocation
    jobDescription
    minSalary
    maxSalary
    offersEquity
    paidRelocation
    visaSponsor
    applyLink
    displayLogo
    highlightJob
    userId
    publishedAt
    company {
      ...companyFields
    }
    regions {
      id
      slug
      regionName
    }
    tags {
      id
      tagName
    }
    errors {
      key
      message
    }
  }

  ${COMPANY_FIELDS_FRAGMENT}
`;

const USER_FIELDS_FRAGMENT = gql`
  fragment userFields on User {
    id
    name
    email
    role
    jobs {
      ...jobFields
    }
    companies {
      ...companyFields
    }
    errors {
      key
      message
    }
  }

  ${JOB_FIELDS_FRAGMENT}
  ${COMPANY_FIELDS_FRAGMENT}
`;

export {
  USER_FIELDS_FRAGMENT as default,
  JOB_FIELDS_FRAGMENT,
  COMPANY_FIELDS_FRAGMENT,
  CHARGE_FIELDS_FRAGMENT
};
