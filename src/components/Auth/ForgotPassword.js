import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Auth } from "aws-amplify";

const ForgotPassword = props => {
  const initialValues = {
    email: ""
  };

  const UserSchema = Yup.object({
    email: Yup.string()
      .email("Invalid email")
      .required("Email is required")
  });

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.forgotPassword(values.email)
      .then(() => {
        props.history.push("/forgotpasswordverification");
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, isSubmitting }) => (
          <Form>
            <div className="container mx-auto w-1/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
              <h1 className="text-lg">Reset your password</h1>
              <p className="help -mt-1 py-2">
                Please enter the email address associated with your account and
                we'll email you a password reset code.
              </p>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="email">
                    Email *
                  </label>
                  <Field
                    name="email"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex items-center justify-between">
                <button
                  type="submit"
                  className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                  disabled={isSubmitting}
                >
                  Send Code
                </button>
                <NavLink
                  to="/signin"
                  className="cursor-pointer text-primary-700 hover:text-primary-0 hover:no-underline"
                >
                  Back to Sign In
                </NavLink>
              </div>
              {errors.message && (
                <div className="flex flex-wrap -mx-3 px-3 pt-3">
                  <p className="error">{errors.message}</p>
                </div>
              )}
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default withRouter(ForgotPassword);
