import React from "react";
import { Auth } from "aws-amplify";

import { UserContext } from "../../context/UserContext";
import SignInForm from "./SignInForm";

const RouteGuard = ({ children }) => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.signIn(values.email, values.password)
      .then(user => {
        setCurrentUser(user);
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  if (!currentUser) {
    return (
      <>
        <div className="container mx-auto w-full flex flex-col justify-center">
          <p className="text-primary-0 text-lg text-center py-20">
            Please Sign In before continuing
          </p>
        </div>
        <SignInForm handleSubmit={handleSubmit} />
      </>
    );
  }

  return children;
};

export default RouteGuard;
