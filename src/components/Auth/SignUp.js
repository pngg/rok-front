import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Auth } from "aws-amplify";

import { UserContext } from "../../context/UserContext";

const SignUp = props => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  const initialValues = {
	username: "",
    email: "",
    password: "",
    confirmPassword: ""
  };

  const UserSchema = Yup.object({
	username: Yup.string()
      .required("Username is required"),
    email: Yup.string()
      .email("Invalid email"),
    password: Yup.string()
      .required("Password is required")
      .min(8, "Password is too short - should be 8 chars minimum.")
      .matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Password must contain numbers, special characters, uppercase and lowercase letters."
      ),
    confirmPassword: Yup.string()
      .required("Confirm Password is required")
      .oneOf([Yup.ref("password")], "Passwords must match")
  });

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.signUp({
      username: values.username,
      password: values.password,
      attributes: {
        email: values.email,
        role: "user",
        name: ""
      }
    })
      .then(res => {
        props.history.push("/welcome");
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, isSubmitting }) => (
          <Form>
            <div className="container mx-auto w-1/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
              <h1 className="text-lg">Create a new account</h1>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="username">
                    Username *
                  </label>
                  <Field
                    name="username"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="username"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
			  <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="email">
                    Email
                  </label>
                  <Field
                    name="email"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="password">
                    Password *
                  </label>
                  <Field
                    name="password"
                    type="password"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="confirmPassword">
                    Confirm Password *
                  </label>
                  <Field
                    name="confirmPassword"
                    type="password"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="confirmPassword"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex items-center justify-between">
                <button
                  type="submit"
                  className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                  disabled={isSubmitting}
                >
                  Create Account
                </button>
                <p className="text-grey-dark text-xs">
                  Have an account?{" "}
                  <NavLink
                    to="/signin"
                    className="cursor-pointer text-primary-700 hover:text-primary-0 hover:no-underline"
                  >
                    Sign in
                  </NavLink>
                </p>
              </div>
              {errors.message && (
                <div className="flex flex-wrap -mx-3 px-3 pt-3">
                  <p className="error">{errors.message}</p>
                </div>
              )}
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default withRouter(SignUp);
