import React from "react";
import { Auth } from "aws-amplify";
import { withRouter } from "react-router-dom";
import { FaSignOutAlt } from "react-icons/fa";

import { UserContext } from "../../context/UserContext";

const SignOut = props => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  const handleSignOut = () => {
    Auth.signOut()
      .then(() => {
        setCurrentUser(null);
        props.history.push("/");
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <>
      <button
        type="button"
        // className="text-primary-700 hover:text-primary-0 mr-1"
        onClick={handleSignOut}
      >
        Signout
      </button>
      {/* <span className="text-primary-700 hover:text-primary-0">
          <FaSignOutAlt />
        </span> */}
    </>
  );
};

export default withRouter(SignOut);
