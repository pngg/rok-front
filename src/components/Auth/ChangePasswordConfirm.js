import React from "react";

const ChangePasswordConfirm = () => {
  return (
    <>
      <div className="container mx-auto w-full flex flex-col justify-center">
        <p className="text-primary-0 text-xl text-center py-20">
          Your password has been successfully updated
        </p>
      </div>
    </>
  );
};

export default ChangePasswordConfirm;
