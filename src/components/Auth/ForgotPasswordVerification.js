import React from "react";
import { withRouter } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Auth } from "aws-amplify";

const ForgotPasswordVerification = props => {
  const initialValues = {
    email: "",
    confirmationCode: "",
    newPassword: ""
  };

  const UserSchema = Yup.object({
    email: Yup.string()
      .email("Invalid email")
      .required("Email is required"),
    newPassword: Yup.string().required("Password is required"),
    confirmationCode: Yup.string().required("Confirmation code is required")
  });

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.forgotPasswordSubmit(
      values.email,
      values.confirmationCode,
      values.newPassword
    )
      .then(() => {
        props.history.push("/changepasswordconfirm");
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, isSubmitting }) => (
          <Form>
            <div className="container mx-auto w-1/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
              <h1 className="text-lg">Change your password</h1>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="confirmationCode">
                    Confirmation code *
                  </label>
                  <Field
                    name="confirmationCode"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <p className="help">
                    Enter confirmation code sent to your email address
                  </p>
                  <ErrorMessage
                    name="confirmationCode"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="email">
                    Email *
                  </label>
                  <Field
                    name="email"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="newPassword">
                    New password *
                  </label>
                  <Field
                    name="newPassword"
                    type="password"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="newPassword"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex items-center justify-between">
                <button
                  type="submit"
                  className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                  disabled={isSubmitting}
                >
                  Submit
                </button>
              </div>
              {errors.message && (
                <div className="flex flex-wrap -mx-3 px-3 pt-3">
                  <p className="error">{errors.message}</p>
                </div>
              )}
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default withRouter(ForgotPasswordVerification);
