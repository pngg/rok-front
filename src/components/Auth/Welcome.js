import React from "react";
import { withRouter } from "react-router";

const Welcome = props => {
  return (
    <>
      <div className="container mx-auto w-1/2 flex flex-col justify-center">
        <p className="text-primary-0 text-xl text-center py-20">
          Welcome. We have sent an email with the confirmation link to the email
          address you provided. Kindly click on the link to confirm your email
          address.
        </p>
      </div>
    </>
  );
};

export default withRouter(Welcome);
