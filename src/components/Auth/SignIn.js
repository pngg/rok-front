import React from "react";
import { withRouter } from "react-router-dom";
import { Auth } from "aws-amplify";

import { UserContext } from "../../context/UserContext";
import SignInForm from "./SignInForm";

const SignIn = props => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.signIn(values.username, values.password)
      .then(user => {
        setCurrentUser(user);
        props.history.push("/account");
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  return (
    <>
      <SignInForm handleSubmit={handleSubmit} />
    </>
  );
};

export default withRouter(SignIn);
