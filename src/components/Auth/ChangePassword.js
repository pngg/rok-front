import React from "react";
import { Auth } from "aws-amplify";
import { withRouter, NavLink } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

import RouteGuard from "./RouteGuard";

const ChangePassword = props => {
  const initialValues = {
    oldPassword: "",
    newPassword: "",
    confirmPassword: ""
  };

  const UserSchema = Yup.object({
    oldPassword: Yup.string().required("Old Password is required"),
    newPassword: Yup.string()
      .required("New Password is required")
      .min(8, "Password is too short - should be 8 chars minimum.")
      .matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Password must contain numbers, special characters, uppercase and lowercase letters."
      ),
    confirmPassword: Yup.string()
      .required("Confirm Password is required")
      .test("passwords-match", "Passwords must match", function(value) {
        return this.parent.newPassword === value;
      })
  });

  const handleSubmit = (values, { setErrors, setSubmitting }) => {
    Auth.currentAuthenticatedUser()
      .then(user => {
        return Auth.changePassword(user, values.oldPassword, values.newPassword)
          .then(() => {
            props.history.push("/changepasswordconfirm");
          })
          .catch(error => {
            let err = null;
            error.message ? (err = error) : (err = { message: error });
            setErrors(err);
            setSubmitting(false);
          });
      })
      .then(data => {
        console.log(data);
      })
      .catch(error => {
        let err = null;
        error.message ? (err = error) : (err = { message: error });
        setErrors(err);
        setSubmitting(false);
      });
  };

  return (
    <>
      <RouteGuard>
        <Formik
          initialValues={initialValues}
          validationSchema={UserSchema}
          onSubmit={handleSubmit}
        >
          {({ errors, isSubmitting }) => (
            <Form>
              <div className="container mx-auto w-1/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
                <h1 className="text-lg">Change your password</h1>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full px-3 mb-6 md:mb-0">
                    <label className="label" htmlFor="oldPassword">
                      Old Password *
                    </label>
                    <Field
                      name="oldPassword"
                      type="password"
                      className="input focus:outline-none focus:bg-white focus:border-gray-500"
                    />
                    <ErrorMessage
                      name="oldPassword"
                      component="div"
                      className="error"
                    />
                  </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full px-3 mb-6 md:mb-0">
                    <label className="label" htmlFor="newPassword">
                      New Password *
                    </label>
                    <Field
                      name="newPassword"
                      type="password"
                      className="input focus:outline-none focus:bg-white focus:border-gray-500"
                    />
                    <ErrorMessage
                      name="newPassword"
                      component="div"
                      className="error"
                    />
                  </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                  <div className="w-full px-3 mb-6 md:mb-0">
                    <label className="label" htmlFor="confirmPassword">
                      Confirm New Password *
                    </label>
                    <Field
                      name="confirmPassword"
                      type="password"
                      className="input focus:outline-none focus:bg-white focus:border-gray-500"
                    />
                    <ErrorMessage
                      name="confirmPassword"
                      component="div"
                      className="error"
                    />
                  </div>
                </div>
                <div className="flex items-center justify-between">
                  <button
                    type="submit"
                    className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                    disabled={isSubmitting}
                  >
                    Change Password
                  </button>
                  <button
                    type="button"
                    className="cursor-pointer text-primary-700 hover:text-primary-0 hover:no-underline"
                    onClick={() => props.history.goBack()}
                  >
                    Cancel
                  </button>
                </div>
                {errors.message && (
                  <div className="flex flex-wrap -mx-3 px-3 pt-3">
                    <p className="error">{errors.message}</p>
                  </div>
                )}
              </div>
            </Form>
          )}
        </Formik>
      </RouteGuard>
    </>
  );
};

export default withRouter(ChangePassword);
