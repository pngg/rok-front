import React from "react";
import { NavLink } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const SignInForm = props => {
  const initialValues = {
    username: "",
    password: ""
  };

  const UserSchema = Yup.object({
	username: Yup.string()
      .required("Username is required"),
    password: Yup.string().required("Password is required")
  });

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={UserSchema}
        onSubmit={props.handleSubmit}
      >
        {({ errors, isSubmitting }) => (
          <Form>
            <div className="container mx-auto w-1/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
              <h1 className="text-lg">Sign in to your account</h1>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="username">
                    Username *
                  </label>
                  <Field
                    name="username"
                    type="text"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <ErrorMessage
                    name="username"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full px-3 mb-6 md:mb-0">
                  <label className="label" htmlFor="password">
                    Password *
                  </label>
                  <Field
                    name="password"
                    type="password"
                    className="input focus:outline-none focus:bg-white focus:border-gray-500"
                  />
                  <p className="help">
                    Forgot your password?{" "}
                    <NavLink
                      to="/forgotpassword"
                      className="text-primary-700 cursor-pointer hover:text-primary-0 hover:no-underline"
                    >
                      Reset password
                    </NavLink>
                  </p>
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="error"
                  />
                </div>
              </div>
              <div className="flex items-center justify-between">
                <button
                  type="submit"
                  className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                  disabled={isSubmitting}
                >
                  Sign In
                </button>
                <p className="text-grey-dark text-xs">
                  No Account?{" "}
                  <NavLink
                    to="/signup"
                    className="cursor-pointer text-primary-700 hover:text-primary-0 hover:no-underline"
                  >
                    Create account
                  </NavLink>
                </p>
              </div>
              {errors.message && (
                <div className="flex flex-wrap -mx-3 px-3 pt-3">
                  <p className="error">{errors.message}</p>
                </div>
              )}
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default SignInForm;
