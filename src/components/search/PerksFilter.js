import React from 'react'
import PropTypes from "prop-types";
import {Formik, Form, Field } from 'formik'

const SearchCriteria = (props) => {
  const initialValues = {
    keyword: "",
    location: ""
  };

  const { criteria, onChange } = props

  const notifyOnChange = newCriteria => {
    onChange(newCriteria);
  };

  const handleSubmit = (values) => {
    console.log(values)

    const newCriteria = {
      ...criteria,
      keyword: values.keyword,
      location: values.location
    };

    notifyOnChange(newCriteria);
  }

  return (
    <Formik initialValues={initialValues}
      onSubmit={handleSubmit}
    >
      <Form>
        <h4>Keyword</h4>
        <div className="input-group">
          <div className="input-group-prepend">
            <div className="input-group-text">
              <i className="fas fa-search" />
            </div>
          </div>
          <Field
            type="search"
            name="keyword"
            placeholder="Search all jobs"
            autoFocus
          />
        </div>
        <h4>Where</h4>
        <div className="input-group">
          <div className="input-group-prepend">
            <div className="input-group-text">
              <i className="fas fa-search" />
            </div>
          </div>
          <Field
            type="search"
            name="location"
            placeholder="Located anywhere"
          />
        </div>
        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
}

SearchCriteria.propTypes = {
    criteria: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

export default SearchCriteria
