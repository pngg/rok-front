import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";

// https://github.com/EpkCloud/react-indeterminate-checkbox#readme

const ThreeStateCheckbox = props => {
  const el = useRef(null);
  const [status, setStatus] = useState({ status: props.status });

  const {onChange, name} = props
  // console.log(props)
  useEffect(() => {
    const updateElement = () => {
      switch (status) {
        case 1:
          el.current.indeterminate = false;
          el.current.checked = true;
          break;
        case 2:
          el.current.indeterminate = false;
          el.current.checked = false;
          break;
        default:
          el.current.indeterminate = true;
          el.current.checked = false;
          break;
      }
    };

    const notifyOnChange = () => {
      if (onChange) {
        onChange(name, status);
      }
    };

    updateElement();
    notifyOnChange();
  }, [status, name, onChange]);

  const handleChange = event => {
    event.persist();

    if (status < 2) {
      return setStatus({ status: status + 1 });
    }
    return setStatus({ status: 0 });
  };

  return (
    <div className="three-state-checkbox">
      <input
        type="checkbox"
        id={props.name}
        ref={el}
        onChange={e => handleChange(e)}
      />
      <label htmlFor={props.name}>{props.label}</label>
    </div>
  );
};

ThreeStateCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  status: PropTypes.number,
  onChange: PropTypes.func
};

ThreeStateCheckbox.defaultProps = {
  status: 0
};

export default ThreeStateCheckbox;
