import React from "react";

import { TitleContext } from "../../context/TitleContext";
import { UserContext } from "../../context/UserContext";

const JobPostList = () => {
  const [title, setTitle] = React.useContext(TitleContext);
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  React.useEffect(() => {
    setTitle("Manage Posts");

    return () => {
      setTitle("My Account");
    };
  }, [title]);

  return (
    <>
      {currentUser && (
        <>
          <div className="w-full flex">
            <h2 className="subtitle">Total jobs {}</h2>
            List of my job posts
          </div>
        </>
      )}
    </>
  );
};

export default JobPostList;
