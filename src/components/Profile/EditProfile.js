import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import ClipLoader from "react-spinners/ClipLoader";

import FileUpload from "../../utils/upload/FileUpload";

import { TitleContext } from "../../context/TitleContext";
import { UserContext } from "../../context/UserContext";

const EditAccount = () => {
  const [title, setTitle] = React.useContext(TitleContext);
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  React.useEffect(() => {
    setTitle("Account Settings");

    return () => {
      setTitle("My Account");
    };
  }, [title]);

  const initialValues = {
    name: "",
    email: "",
    username: "",
    picture: {}
  };

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/gif",
    "image/png"
  ];

  const UserSchema = Yup.object({
    name: Yup.string(),
    email: Yup.string().email("Invalid email"),
    preferred_username: Yup.string(),
    picture: Yup.mixed()
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });

  const handleSubmit = () => {};

  return (
    <>
      {currentUser && (
        <Formik
          initialValues={initialValues}
          validationSchema={UserSchema}
          onSubmit={handleSubmit}
        >
          {({ errors, isSubmitting }) => {
            let content = null;
            !isSubmitting
              ? (content = (
                  <Form>
                    <div className="flex flex-wrap -mx-3 mb-6">
                      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="label" htmlFor="name">
                          Name
                        </label>
                        <Field
                          name="name"
                          type="text"
                          placeholder={currentUser.attributes.name}
                          className="input focus:outline-none focus:bg-white focus:border-gray-500"
                          autoFocus
                        />
                        <ErrorMessage
                          name="name"
                          component="div"
                          className="error"
                        />
                      </div>
                      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="label" htmlFor="email">
                          Email
                        </label>
                        <Field
                          name="email"
                          type="text"
                          placeholder={currentUser.attributes.email}
                          className="input focus:outline-none focus:bg-white focus:border-gray-500"
                        />
                        <ErrorMessage
                          name="email"
                          component="div"
                          className="error"
                        />
                      </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="label" htmlFor="preferred_username">
                          Username
                        </label>
                        <Field
                          name="preferred_username"
                          type="text"
                          placeholder={
                            currentUser.attributes.preferred_username
                          }
                          className="input focus:outline-none focus:bg-white focus:border-gray-500"
                        />
                        <ErrorMessage
                          name="preferred_username"
                          component="div"
                          className="error"
                        />
                      </div>
                      <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="label" htmlFor="picture">
                          Profile picture
                        </label>
                        {/* <Field name="companyLogo">
                          {({ field, form }) => (
                            <FileUpload field={field} form={form} />
                          )}
                        </Field> */}
                        <div className="overflow-hidden relative w-64 mt-4 mb-4">
                          <button className="bg-tertiary-lighter hover:bg-tertiary text-primary-0 font-bold py-2 px-4 w-full inline-flex items-center">
                            <svg
                              fill="#000"
                              height="18"
                              viewBox="0 0 24 24"
                              width="18"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path d="M0 0h24v24H0z" fill="none" />
                              <path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z" />
                            </svg>
                            <span className="ml-2">Change profile picture</span>
                          </button>
                          <input
                            className="cursor-pointer absolute block py-2 px-4 w-full opacity-0 pin-r pin-t"
                            type="file"
                            name="picture"
                            accept="image/*"
                          />
                        </div>
                        <ErrorMessage
                          name="picture"
                          component="div"
                          className="error"
                        />
                      </div>
                    </div>
                    <div className="flex items-center justify-end">
                      <button
                        type="submit"
                        className="bg-primary-700 hover:bg-primary-0 text-white font-bold py-2 px-4 rounded"
                        disabled={isSubmitting}
                      >
                        Update Account
                      </button>
                    </div>
                    {errors.updateUserError && (
                      <div className="flex flex-wrap -mx-3 px-3 pt-3">
                        <p className="error">{errors.updateUserError}</p>
                      </div>
                    )}
                  </Form>
                ))
              : (content = (
                  <div className="flex justify-center items-center py-12">
                    <ClipLoader
                      // css={{}}
                      size={150}
                      color={"#003E6B"}
                      loading={true}
                    />
                  </div>
                ));

            return content;
          }}
        </Formik>
      )}
    </>
  );
};

export default EditAccount;
