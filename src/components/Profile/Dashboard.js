import React from "react";
import { Route, NavLink } from "react-router-dom";

import { UserContext } from "../../context/UserContext";

const Dashboard = () => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);

  return (
    <>
      {currentUser && (
        <>
          <div className="w-full">
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="label">Name</label>
                <span className="input">
                  {currentUser.attributes.name || "-"}
                </span>{" "}
              </div>
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="label">Email</label>
                <span className="input">
                  {currentUser.attributes.email || "-"}
                </span>{" "}
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full px-3 mb-6 md:mb-0">
                <label className="label">username</label>
                <span className="input">
                  {currentUser.attributes.preferred_username || "-"}
                </span>{" "}
              </div>
            </div>
            <div className="flex flex-wrap justify-end px-3">
              <NavLink to="/account/settings" className="">
                Edit
              </NavLink>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Dashboard;
