import React from "react";
import { IconContext } from "react-icons";
import { FaCheckCircle } from "react-icons/fa";

import { PriceContext } from "../../../context/PriceContext";
import { JobContext } from "../../../context/JobContext";
import { StandardPackageContext } from "../../../context/StandardPackageContext";
import { FeaturedPackageContext } from "../../../context/FeaturedPackageContext";

const SelectPackage = () => {
  const [totalPrice, setTotalPrice] = React.useContext(PriceContext);
  const [jobData, setJobData] = React.useContext(JobContext);
  const [selectStandard, setSelectStandard] = React.useContext(
    StandardPackageContext
  );
  const [selectFeatured, setSelectFeatured] = React.useContext(
    FeaturedPackageContext
  );

  const handleStandardClick = () => {
    setSelectStandard(true);
    setSelectFeatured(false);
    setTotalPrice(14900);
    setJobData({ ...jobData, highlightJob: false, displayLogo: true });
  };

  const handleFeaturedClick = () => {
    setSelectFeatured(true);
    setSelectStandard(false);
    setTotalPrice(19900);
    setJobData({ ...jobData, highlightJob: true, displayLogo: true });
  };

  return (
    <>
      <h1 className="w-full text-center">Choose a package</h1>
      <div className="flex justify-center">
        <div className="w-full sm:w-1/2 py-4 px-4">
          <div
            className={
              selectStandard
                ? "bg-white relative shadow shadow-xl border-b-4 rounded-b-lg border-vivid-700 rounded-lg text-gray-800 hover:shadow-lg cursor-pointer"
                : "bg-white relative shadow rounded-lg text-gray-800 hover:shadow-lg cursor-pointer"
            }
            onClick={handleStandardClick}
          >
            <div className="right-0 mt-4 rounded-l-full absolute text-center font-bold text-xs text-white px-2 py-1 bg-orange-500">
              Standard
            </div>
            <div className="flex justify-center bg-primary-0 rounded-t-lg h-20">
              <div className="py-4 mr-2 h-12 w-16 text-3xl text-white font-bold font-title">
                <span className="text-xl">$</span>149
              </div>
            </div>
            <div className="flex flex-col py-2 px-2">
              <IconContext.Provider value={{ className: "text-primary-0" }}>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">List your ad for 4 weeks</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Display your company logo</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Tweet by @Remote_OK_Jobs</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Email to followers</span>
                </div>
              </IconContext.Provider>
              <IconContext.Provider value={{ className: "text-gray-300" }}>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Highlight your ad</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Pin ad to the top of the board</span>
                </div>
              </IconContext.Provider>
            </div>
          </div>
        </div>
        <div className="w-full sm:w-1/2 py-4 px-4">
          <div
            className={
              selectFeatured
                ? "bg-white relative shadow shadow-xl border-b-4 rounded-b-lg border-vivid-700 rounded-lg text-gray-800 hover:shadow-lg cursor-pointer"
                : "bg-white relative shadow rounded-lg text-gray-800 hover:shadow-lg cursor-pointer"
            }
            onClick={handleFeaturedClick}
          >
            <div className="right-0 mt-4 rounded-l-full absolute text-center font-bold text-xs text-white px-2 py-1 bg-orange-500">
              Featured
            </div>
            <div className="flex justify-center bg-primary-0 rounded-t-lg h-20">
              <div className="py-4 mr-2 h-12 w-16 text-3xl text-white font-bold font-title">
                <span className="text-xl">$</span>199
              </div>
            </div>
            <div className="flex flex-col py-2 px-2">
              <IconContext.Provider value={{ className: "text-primary-0" }}>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">List your ad for 4 weeks</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Display your company logo</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Tweet by @Remote_OK_Jobs</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Email to followers</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Highlight your ad</span>
                </div>
                <div className="inline-flex items-center text-sm font-light text-center my-2">
                  <FaCheckCircle />
                  <span className="ml-2">Pin ad to the top of the board</span>
                </div>
              </IconContext.Provider>
            </div>
          </div>
        </div>
      </div>
      <div className="flex mt-10">
        <div className="w-full text-center">
          <p className="text-black-500 font-bold text-xl">
            Your total is:{" "}
            <span className="text-green-700 font-extrabold text-2xl italic">
              ${totalPrice / 100}
            </span>
          </p>
        </div>
      </div>
    </>
  );
};

export default SelectPackage;
