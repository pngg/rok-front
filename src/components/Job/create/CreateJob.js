import React from "react";
import { withRouter } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { Formik, Form } from "formik";
import { Elements } from "react-stripe-elements";

import RouteGuard from "../../Auth/RouteGuard";
import JobDetailsForm from "./JobDetailsForm";
import CompanyDetailsForm from "./CompanyDetailsForm";
import PreviewJob from "./PreviewJob";
import SelectPackage from "./SelectPackage";
import Checkout from "../../Order/Checkout";
import JobSchema from "../../../utils/formik/job/schema";
import initialValues from "../../../utils/formik/job/initialValues";

import { StripePropsContext } from "../../../context/StripePropsContext";
import { ThumbContext } from "../../../context/ThumbContext";
import { PriceContext } from "../../../context/PriceContext";
import { JobContext } from "../../../context/JobContext";
import { StandardPackageContext } from "../../../context/StandardPackageContext";
import { FeaturedPackageContext } from "../../../context/FeaturedPackageContext";

// Queries
import { CREATE_JOB_DRAFT_MUTATION } from "../../../graphql/mutations";
import { CREATE_JOB_MUTATION } from "../../../graphql/mutations";

const pages = [
  <JobDetailsForm />,
  <CompanyDetailsForm />,
  <PreviewJob />,
  <SelectPackage />,
  <Checkout />
];

const CreateJob = props => {
  const [page, setPage] = React.useState(0);
  const [jobData, setJobData] = React.useContext(JobContext);
  const [stripeProps, setStripeProps] = React.useContext(StripePropsContext);
  const [thumb, setThumb] = React.useContext(ThumbContext);
  const [totalPrice, setTotalPrice] = React.useContext(PriceContext);
  const [selectStandard, setSelectStandard] = React.useContext(
    StandardPackageContext
  );
  const [selectFeatured, setSelectFeatured] = React.useContext(
    FeaturedPackageContext
  );
  const [createJobDraft] = useMutation(CREATE_JOB_DRAFT_MUTATION);
  const [createJob] = useMutation(CREATE_JOB_MUTATION);

  const handleSetPage = async values => {
    const regions =
      values.regions.length > 0
        ? values.regions.map(region => region.value)
        : [];

    const jobType = values.jobType.value || "full-time";

    setPage(page + 1);
    setJobData({
      ...values,
      regions,
      jobType
    });
  };

  console.log({jobData});
  

  const handleResetPage = () => {
    setPage(0);
  };

  const handleChangePlan = () => {
    setPage(page - 1);
  };

  const handleCreateJobResponse = (res, setErrors) => {
    setThumb(null);
    setSelectStandard(true);
    setSelectFeatured(false);
    return props.history.push({
      pathname: "/thankyou",
      state: { res }
    });
  };

  const handleSubmit = async (values, { setErrors, setSubmitting }) => {
    try {
      let { token } = await stripeProps.createToken({
        name: values.cardHolderName
      });

      const res = await createJob({
        variables: {
          amount: totalPrice,
          currency: "usd",
          source: token.id,
          description: "Job post on Remote OK",
          ...jobData
        }
      });
      res && handleCreateJobResponse(res, setErrors);
    } catch (err) {
      setErrors({
        tokenError: err.graphQLErrors.map(e => e.message) || err
      });
      setSubmitting(false);
    }
  };

  return (
    <>
      <RouteGuard>
        <Formik
          initialValues={initialValues}
          validationSchema={JobSchema}
          onSubmit={handleSubmit}
        >
          {({ values, dirty, errors, isValid, isSubmitting }) => {
            return (
              <>
                <section className="hero mt-32 mb-10">
                  <div className="container mx-auto py-24 max-w-4xl">
                    <h1 className="font-bold text-5xl text-center">
                      Reach the best candidates from around the world.
                    </h1>
                  </div>
                </section>

                <section className="bg-white">
                  <Form>
                    {page === pages.length - 3 ? (
                      <div className="flex justify-center items-center mt-10">
                        <h1 className="uppercase text-base tracking-wide text-vivid-0">
                          Job Preview
                        </h1>
                      </div>
                    ) : null}

                    {page === pages.length - 1 ? (
                      <>
                        <Elements>
                          <Checkout
                            isSubmitting={isSubmitting}
                            isValid={isValid}
                            errors={errors}
                          />
                        </Elements>
                        {!isSubmitting && (
                          <div className="flex flex-col justify-center items-center my-2 mx-auto">
                            <div className="container mx-auto sm:w-1/2 bg-white p-2">
                              <div className="flex flex-col items-center">
                                <button
                                  type="button"
                                  onClick={handleChangePlan}
                                  className="text-vivid-500 hover:text-vivid-700 italic"
                                >
                                  Review package?
                                </button>
                              </div>
                            </div>
                            <div className="help text-center">
                              Payments are in USD and processed by Stripe using
                              secure SSL/TLS
                            </div>
                          </div>
                        )}
                      </>
                    ) : (
                      <div className="w-full max-w-5xl shadow-md rounded p-8 my-4 mx-auto">
                        {pages[page]}
                      </div>
                    )}

                    {page === pages.length - 2 ? (
                      <div className="flex justify-center items-center p-8 mx-auto text-centered">
                        <button
                          type="button"
                          className="bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 rounded disabled:opacity-75 disabled:cursor-not-allowed"
                          onClick={() => handleSetPage(values)}
                          disabled={isSubmitting || !dirty}
                        >
                          Proceed to Payment
                        </button>
                        <span className="text-xs font-bold py-2 mx-5">or</span>
                        <button
                          type="button"
                          onClick={handleResetPage}
                          className="text-vivid-500 hover:text-vivid-700 font-bold"
                        >
                          Make changes
                        </button>
                      </div>
                    ) : page === pages.length - 3 ? (
                      <div className="flex justify-center items-center p-8 mx-auto text-centered">
                        <button
                          type="button"
                          className="bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 rounded disabled:opacity-75 disabled:cursor-not-allowed"
                          onClick={() => handleSetPage(values)}
                          disabled={isSubmitting || !dirty}
                        >
                          Next page
                        </button>
                        <span className="text-xs font-bold py-2 mx-5">or</span>
                        <button
                          type="button"
                          onClick={handleResetPage}
                          className="text-vivid-500 hover:text-vivid-700 font-bold"
                        >
                          Make changes
                        </button>
                      </div>
                    ) : page === pages.length - 4 ? (
                      <div className="flex justify-center items-center p-8 mx-auto text-centered">
                        <button
                          type="button"
                          className="bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 rounded disabled:opacity-75 disabled:cursor-not-allowed"
                          onClick={() => handleSetPage(values)}
                          disabled={
                            !dirty ||
                            errors.companyName ||
                            errors.companyMission ||
                            errors.companyLogo ||
                            errors.companyUrl ||
                            errors.companyDescription
                          }
                        >
                          Next page
                        </button>
                        <span className="text-xs font-bold py-2 mx-5">or</span>
                        <button
                          type="button"
                          onClick={handleResetPage}
                          className="text-vivid-500 hover:text-vivid-700 font-bold"
                        >
                          Make changes
                        </button>
                      </div>
                    ) : page === pages.length - 5 ? (
                      <div className="flex justify-center items-center p-8 mx-auto text-centered">
                        <button
                          type="button"
                          className="bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 rounded disabled:opacity-75 disabled:cursor-not-allowed"
                          onClick={() => handleSetPage(values)}
                          disabled={
                            !dirty ||
                            errors.jobTitle ||
                            errors.jobType ||
                            errors.jobDescription ||
                            errors.regions ||
                            errors.applyLink
                          }
                        >
                          Next page
                        </button>
                      </div>
                    ) : null}
                  </Form>
                </section>
              </>
            );
          }}
        </Formik>
      </RouteGuard>
    </>
  );
};

export default withRouter(CreateJob);
