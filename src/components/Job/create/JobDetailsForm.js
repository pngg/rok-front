import React from "react";
import { Field, ErrorMessage } from "formik";
import { fromHTML } from "@remirror/core";
import Select from "react-select";

import { ProseEditor } from "../../../utils/editor/ProseEditor";
import { selectCustomStyles } from "../../../helpers";

const JobDetailsForm = () => {
  const jobTypeOptions = [
    { label: "Full-time", value: "full-time" },
    { label: "Part-time", value: "part-time" },
    { label: "Contract", value: "contract" },
    { label: "Internship", value: "internship" }
  ];

  const regionOptions = [
    { label: "Africa", value: "Africa" },
    { label: "Middle East", value: "Middle East" },
    { label: "Europe", value: "Europe" },
    { label: "Asia/Pacific", value: "Asia Pacific" },
    { label: "South America", value: "South America" },
    { label: "North America", value: "North America" },
    { label: "Anywhere", value: "Anywhere" }
  ];

  return (
    <>
      <div className="flex justify-between mb-5">
        <h1 className="uppercase text-base tracking-wide text-vivid-0">
          Tell us about the position
        </h1>
        <span className="mb-6 italic text-vivid-0">Required fields *</span>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="jobTitle">
            Job Title *
          </label>
          <Field
            name="jobTitle"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
            autoFocus
          />
          <p className="help">Example: "Full-Stack Developer"</p>
          <ErrorMessage name="jobTitle" component="div" className="error" />
        </div>

        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="jobType">
            Job Type *
          </label>
          <Field name="jobType">
            {({ field, form }) => (
              <Select
                name={field.name}
                defaultValue={jobTypeOptions[0]}
                value={form.values.jobType}
                error={form.errors.jobType}
                touched={form.touched.jobType}
                onChange={value => {
                  form.setFieldValue(field.name, value);
                  console.log({value})
                }}
                onBlur={() => {
                  form.setFieldTouched(field.name, true);
                }}
                placeholder="Full-time"
                options={jobTypeOptions}
                styles={selectCustomStyles}
                menuPortalTarget={document.body}
              />
            )}
          </Field>
          <ErrorMessage name="jobType" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="jobLocation">
            Location
          </label>
          <Field
            name="jobLocation"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Example: "Bavaria, Germany"</p>
          <ErrorMessage name="jobLocation" component="div" className="error" />
        </div>

        <div className="w-full md:w-1/2 px-3">
          <label className="label" htmlFor="regions">
            Region *
          </label>
          <Field name="regions">
            {({ field, form }) => (
              <Select
                name={field.name}
                value={form.values.regions}
                error={form.errors.regions}
                touched={form.touched.regions}
                onChange={value => {
                  form.setFieldValue(field.name, value);
                }}
                onBlur={() => {
                  form.setFieldTouched(field.name, true);
                }}
                options={regionOptions}
                isMulti={true}
                styles={selectCustomStyles}
                menuPortalTarget={document.body}
              />
            )}
          </Field>
          <ErrorMessage name="regions" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="tags">
            Tags
          </label>
          <Field
            name="tags"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">
            Use tags like industry and tech stack, and separate multiple tags by
            comma e.g. (Big Data, NLP, Numpy, Tensorflow)
          </p>
          <ErrorMessage name="tags" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="jobDescription">
            Job Description *
          </label>
		  /* Got this error --> RangeError: Adding different instances of a keyed plugin (plugin$1)
		  so removed ProseEditor field*/
          <ErrorMessage
            name="jobDescription"
            component="div"
            className="error"
          />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="applyLink">
            Apply Link *
          </label>
          <Field
            name="applyLink"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
            placeholder="https://company.co/jobs/apply or hello@company.co"
          />
          <p className="help">Enter URL link or an email address</p>
          <ErrorMessage name="applyLink" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="minSalary">
            Min. Annual Salary Range
          </label>
          <Field
            name="minSalary"
            type="number"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Enter min. annual salary range. (Optional)</p>
          <ErrorMessage name="minSalary" component="div" className="error" />
        </div>

        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="maxSalary">
            Max. Annual Salary Range
          </label>
          <Field
            name="maxSalary"
            type="number"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Enter max. annual salary range. (Optional)</p>
          <ErrorMessage name="maxSalary" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="perks">
            Perks
          </label>
        </div>

        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="offersEquity">
            <Field name="offersEquity">
              {({ field, form }) => (
                <input
                  name={field.name}
                  type="checkbox"
                  checked={form.values.offersEquity}
                  onChange={form.handleChange}
                  onBlur={form.handleBlur}
                  className="form-checkbox"
                />
              )}
            </Field>{" "}
            Offers Equity
          </label>
          <ErrorMessage name="offersEquity" component="div" className="error" />
        </div>

        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="visaSponsor">
            <Field name="visaSponsor">
              {({ field, form }) => (
                <input
                  name={field.name}
                  type="checkbox"
                  checked={form.values.visaSponsor}
                  onChange={form.handleChange}
                  onBlur={form.handleBlur}
                  className="form-checkbox"
                />
              )}
            </Field>{" "}
            Visa Sponsor
          </label>
          <ErrorMessage name="visaSponsor" component="div" className="error" />
        </div>

        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="paidRelocation">
            <Field name="paidRelocation">
              {({ field, form }) => (
                <input
                  name={field.name}
                  type="checkbox"
                  checked={form.values.paidRelocation}
                  onChange={form.handleChange}
                  onBlur={form.handleBlur}
                  className="form-checkbox"
                />
              )}
            </Field>{" "}
            Paid Relocation
          </label>
          <ErrorMessage
            name="paidRelocation"
            component="div"
            className="error"
          />
        </div>
      </div>
    </>
  );
};

export default JobDetailsForm;
