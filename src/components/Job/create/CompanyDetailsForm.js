import React from "react";
import { Field, ErrorMessage } from "formik";
import { fromHTML } from "@remirror/core";

import { ProseEditor } from "../../../utils/editor/ProseEditor";
import FileUpload from "../../../utils/upload/FileUpload";

const CompanyDetailsForm = () => {
  return (
    <>
      <div className="flex justify-between mb-5">
        <h1 className="uppercase text-base tracking-wide text-vivid-0">
          Tell us about your company
        </h1>
        <span className="mb-6 italic text-vivid-0">Required fields *</span>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyName">
            Company Name *
          </label>
          <Field
            name="companyName"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
            autoFocus
          />
          <p className="help">Enter your company or organization’s name.</p>
          <ErrorMessage name="companyName" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyMission">
            Company Mission Statement
          </label>
          <Field
            name="companyMission"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">
            Enter your company or organization’s mission statement for your
            company’s profile.
          </p>
          <ErrorMessage
            name="companyMission"
            component="div"
            className="error"
          />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyLogo">
            Company Logo *
          </label>
          <Field name="companyLogo">
            {({ field, form }) => <FileUpload field={field} form={form} />}
          </Field>
          <ErrorMessage name="companyLogo" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyUrl">
            Company's Website URL *
          </label>
          <Field
            name="companyUrl"
            type="url"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Example: "https://mycompany.com/"</p>
          <ErrorMessage name="companyUrl" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyTwitter">
            Company's Twitter
          </label>
          <Field
            name="companyTwitter"
            type="text"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Enter your company's Twitter</p>
          <ErrorMessage
            name="companyTwitter"
            component="div"
            className="error"
          />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyVideo">
            Company's Video URL
          </label>
          <Field
            name="companyVideo"
            type="url"
            className="input focus:outline-none focus:bg-white focus:border-gray-500"
          />
          <p className="help">Enter URL to your company's video.</p>
          <ErrorMessage name="companyVideo" component="div" className="error" />
        </div>
      </div>

      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full px-3 mb-6 md:mb-0">
          <label className="label" htmlFor="companyDescription">
            Company Description
          </label>
          /* Got this error --> RangeError: Adding different instances of a keyed plugin (plugin$1)
		  so removed ProseEditor field*/
          <ErrorMessage
            name="companyDescription"
            component="div"
            className="error"
          />
        </div>
      </div>
    </>
  );
};

export default CompanyDetailsForm;
