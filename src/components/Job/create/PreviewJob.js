import React from "react";
import { format } from "date-fns";
import {
  TwitterShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  WorkplaceShareButton,
  EmailShareButton,
  TwitterIcon,
  FacebookIcon,
  LinkedinIcon,
  TelegramIcon,
  WhatsappIcon,
  WorkplaceIcon,
  EmailIcon
} from "react-share";

import { formatCurrency, capitalizeWord, purifyHtml } from "../../../helpers";
import { ThumbContext } from "../../../context/ThumbContext";
import useJob from "../../../hooks/useJob";

const PreviewJob = () => {
  const job = useJob();
  const [thumb, setThumb] = React.useContext(ThumbContext);

  const shareUrl = "http://www.remote-ok.com";

  return (
    <>
      {job && (
        <section className="bg-white">
          <div className="container mx-auto bg-white flex flex-wrap">
            <div className="flex flex-wrap md:w-3/4 pr-10">
              <div className="w-full">
                <span className="label">
                  Posted {format(new Date(), "MMM dd")}
                </span>
                <h1 className="text-5xl font-bold">{job.jobTitle}</h1>
                <div className="flex flex-wrap items-center py-2 mb-5 mt-2">
                  {job.jobType && (
                    <span className="help text-yellow-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      {capitalizeWord(job.jobType)}
                    </span>
                  )}
                  {job.minSalary && (
                    <span className="help text-green-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      {job.currency}
                      {formatCurrency(job.minSalary)}
                      {" - "}
                      {formatCurrency(job.maxSalary)} {"/ yr"}
                    </span>
                  )}
                  {job.offersEquity && (
                    <span className="help text-green-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Equity
                    </span>
                  )}
                  {job.visaSponsor && (
                    <span className="help text-red-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Visa sponsor
                    </span>
                  )}
                  {job.paidRelocation && (
                    <span className="help text-blue-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2">
                      Paid relocation
                    </span>
                  )}
                  {job.regions &&
                    job.regions.map((region, index) => (
                      <span
                        key={index}
                        className="help text-blue-500 border-2 border-neutral-200 bg-neutral-200 rounded px-2 mr-2 mb-2"
                      >
                        Remote {region}
                      </span>
                    ))}
                </div>
              </div>
              <div className="w-full">
                <p
                  className=""
                  dangerouslySetInnerHTML={purifyHtml(`${job.jobDescription}`)}
                />
              </div>
              <div className="flex flex-wrap sm:justify-between justify-center sm:items-center w-full mt-12">
                <div className="flex sm:w-2/3">
                  <a
                    href="{job.applyUrl}"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="rounded text-white font-bold bg-primary-500 hover:bg-primary-700 hover:shadow-lg p-3 hover:no-underline cursor-pointer opacity-50 cursor-not-allowed"
                  >
                    Apply for this position
                  </a>
                </div>
                <div className="flex flex-col sm:w-1/3 sm:mt-0 mt-10 justify-end text-center">
                  <h3 className="label">Share this job</h3>
                  <div className="flex justify-center a2a_kit a2a_kit_size_32 a2a_default_style disabled:opacity-50 disabled:cursor-not-allowed">
                    <TwitterShareButton
                      type="button"
                      url={shareUrl}
                      quote={"job.jobTitle"}
                      disabled
                    >
                      <TwitterIcon size={32} round />
                    </TwitterShareButton>
                    <FacebookShareButton
                      type="button"
                      url={shareUrl}
                      quote={job.jobTitle}
                      disabled
                    >
                      <FacebookIcon size={32} round />
                    </FacebookShareButton>
                    <LinkedinShareButton
                      type="button"
                      url={shareUrl}
                      quote={job.jobTitle}
                      disabled
                    >
                      <LinkedinIcon size={32} round />
                    </LinkedinShareButton>
                    <WorkplaceShareButton
                      type="button"
                      url={shareUrl}
                      quote={job.jobTitle}
                      disabled
                    >
                      <WorkplaceIcon size={32} round />
                    </WorkplaceShareButton>
                    <EmailShareButton
                      type="button"
                      url={shareUrl}
                      quote={job.jobTitle}
                      disabled
                    >
                      <EmailIcon size={32} round />
                    </EmailShareButton>
                  </div>
                </div>
              </div>
            </div>

            <div className="md:w-1/4 w-full">
              <div className="flex flex-col justify-center items-center bg-neutral-100 max-w-full rounded overflow-hidden shadow-lg">
                <div className="flex flex-col justify-center items-center w-full text-center p-6 border-b">
                  <div className="pb-2/3">
                    {thumb && (
                      <img
                        className="h-full w-full object-cover rounded-full"
                        src={thumb.dataURL}
                        alt={thumb.file.name}
                      />
                    )}
                  </div>
                  <p className="pt-2 text-lg font-semibold">
                    {job.companyName}
                  </p>
                  <a
                    className="text-sm text-gray-600 hover:no-underline cursor-pointer hover:text-primary-0"
                    href={job.companyUrl}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    View website
                  </a>
                  <div className="mt-5">
                    <a
                      href={job.applyLink}
                      className="border rounded-full py-2 px-4 text-xs font-semibold text-gray-700 hover:no-underline cursor-pointer hover:text-primary-0 opacity-50 cursor-not-allowed"
                    >
                      Apply for this position
                    </a>
                  </div>
                </div>
                <div className="flex justify-center w-full px-6 py-4 text-center">
                  <a
                    className="inline-block rounded-full px-3 py-1 text-xs font-semibold text-gray-600 mr-2 hover:no-underline cursor-pointer hover:text-primary-0"
                    href={`/companies/${job.companyName}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    View more jobs
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default PreviewJob;
