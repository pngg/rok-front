import React, { Suspense } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import { Auth } from "aws-amplify";
import ClipLoader from "react-spinners/ClipLoader";

import Header from "../pages/Header";
import Footer from "../pages/Footer";
import Home from "../pages/Home";
import About from "../pages/About";
import Privacy from "../pages/Privacy";
import Terms from "../pages/Terms";
import Faqs from "../pages/Faqs";
import SignUp from "./Auth/SignUp";
import SignIn from "./Auth/SignIn";
import ChangePassword from "./Auth/ChangePassword";
import ChangePasswordConfirm from "./Auth/ChangePasswordConfirm";
import ForgotPassword from "./Auth/ForgotPassword";
import ForgotPasswordVerification from "./Auth/ForgotPasswordVerification";
import Welcome from "./Auth/Welcome";
import Thankyou from "./Order/Thankyou";
import Account from "../pages/Account";
import CreateJob from "./Job/create/CreateJob";
import Search from "../pages/Search";
import Job from "../pages/Job";
import Company from "../pages/Company";

import ROUTES from "../routes";
import { UserContext } from "../context/UserContext";
import { StripeOverlayContext } from "../context/StripeOverlayContext";
import StripeOverlay from "../utils/StripeOverlay";

const App = () => {
  const [currentUser, setCurrentUser] = React.useContext(UserContext);
  const [stripeOverlay, setStripeOverlay] = React.useContext(
    StripeOverlayContext
  );

  React.useEffect(() => {
    Auth.currentAuthenticatedUser()
      .then(user => {
        setCurrentUser(user);
      })
      .catch(error => {
        console.log(error);
      });
  }, [setCurrentUser]);

  return (
    <Suspense fallback={<ClipLoader />}>
      <StripeOverlay stripe={stripeOverlay}>
        <Header />
        <Switch>
          <Route exact path={ROUTES.home} component={Home} />
          <Route path={ROUTES.about} component={About} />
          <Route path={ROUTES.privacy} component={Privacy} />
          <Route path={ROUTES.terms} component={Terms} />
          <Route path={ROUTES.faqs} component={Faqs} />
          <Route path={ROUTES.signUp} component={SignUp} />
          <Route path={ROUTES.signIn} component={SignIn} />
          <Route path={ROUTES.changePassword} component={ChangePassword} />
          <Route
            path={ROUTES.changePasswordConfirm}
            component={ChangePasswordConfirm}
          />
          <Route path={ROUTES.forgotPassword} component={ForgotPassword} />
          <Route
            path={ROUTES.forgotPasswordVerification}
            component={ForgotPasswordVerification}
          />
          <Route path={ROUTES.welcome} component={Welcome} />
          <Route path={ROUTES.thankyou} component={Thankyou} />
          <Route path={ROUTES.account} component={Account} />
          <Route path={ROUTES.new} component={CreateJob} />
          <Route exact path={ROUTES.jobs} component={Search} />
          <Route path={ROUTES.job} component={Job} />
          <Route exact path={ROUTES.companies} component={Company} />
          <Route path={ROUTES.company} component={Company} />
          <Redirect to={ROUTES.home} />
        </Switch>
        <Footer />
      </StripeOverlay>
    </Suspense>
  );
};

export default withRouter(App);
