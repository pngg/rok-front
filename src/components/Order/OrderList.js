import React from "react";

import { TitleContext } from "../../context/TitleContext";

const OrderList = () => {
  const [title, setTitle] = React.useContext(TitleContext);
  
   React.useEffect(() => {
     setTitle("My Orders");

     return () => {
       setTitle("My Account");
     };
   }, [title]);
  
  return <>Order list</>;
};

export default OrderList;
