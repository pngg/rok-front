import React from "react";
import { Link, NavLink } from "react-router-dom";

const Thankyou = props => {
  return (
    <>
      <section className="bg-white">
        <div className="container mx-auto pt-32">
          <div className="flex flex-wrap justify-center items-center container mx-auto mt-20 mb-5">
            {props.location.state ? (
              <div className="flex flex-wrap justify-center items-center">
                <div className="w-full flex flex-col justify-center items-center">
                  <h1 className="flex justify-center items-center w-full text-5xl mb-10">
                    Order Complete
                  </h1>
                  <p className="w-full text-3xl text-center">
                    Thank you for your order. Your job at{" "}
                    {
                      props.location.state.res.data.createJob.job.company
                        .companyName
                    }{" "}
                    has been successfully posted.
                  </p>
                </div>
                <div className="w-1/2 flex flex-wrap justify-center items-center">
                  <Link
                    to={`/jobs/${props.location.state.res.data.createJob.job.id}-${props.location.state.res.data.createJob.job.slug}`}
                    className="flex justify-center items-center w-full sm:w-1/2 text-2xl text-primary-700 hover:text-primary-0 hover:no-underline mt-5"
                  >
                    View Job
                  </Link>
                  {/* <a
                    href={
                      props.location.state.res.data.createJob.charge.receiptUrl
                    }
                    className="flex justify-center sm:justify-start items-center w-full sm:w-1/2 text-2xl text-primary-700 hover:text-primary-0 hover:no-underline mt-5"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Download receipt
                  </a> */}
                  <Link
                    to="/account"
                    className="flex justify-center items-center w-full sm:w-1/2 text-2xl text-primary-700 hover:text-primary-0 hover:no-underline mt-5"
                  >
                    My Orders
                  </Link>
                </div>
              </div>
            ) : (
              <div className="flex flex-wrap justify-center items-center">
                <NavLink
                  to="/new"
                  className="vivid-button hover:bg-vivid-500 hover:no-underline"
                >
                  Post a new job
                </NavLink>
              </div>
            )}
          </div>
        </div>
      </section>
    </>
  );
};

export default Thankyou;
