import React from "react";
import {
  injectStripe,
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
  PaymentRequestButtonElement,
  IbanElement,
  IdealBankElement
} from "react-stripe-elements";
import { Field, ErrorMessage } from "formik";
import LoadingOverlay from "react-loading-overlay";

import { StripePropsContext } from "../../context/StripePropsContext";
import ClipLoader from "react-spinners/ClipLoader";

const Checkout = props => {
  const [stripeProps, setStripeProps] = React.useContext(StripePropsContext);

  const submit = async () => {
    await setStripeProps(props.stripe);
  };

  var elementStyles = {
    base: {
      color: "#32325D",
      fontWeight: 500,
      fontFamily: "Source Code Pro, Consolas, Menlo, monospace",
      fontSize: "16px",
      fontSmoothing: "antialiased",

      "::placeholder": {
        color: "#CFD7DF"
      },
      ":-webkit-autofill": {
        color: "#e39f48"
      }
    },
    invalid: {
      color: "#E25950",

      "::placeholder": {
        color: "#FFCCA5"
      }
    }
  };

  return (
    <>
      <LoadingOverlay
        active={props.isSubmitting}
        spinner={<ClipLoader size={150} color={"#003E6B"} />}
        text="Processing your payment..."
        styles={{
          wrapper: base => ({
            ...base,
            position: "relative"
          }),
          overlay: base => ({
            ...base,
            color: "#003E6B",
            background: "rgba(255, 255, 255, 1)"
          }),
          content: base => ({
            ...base,
            alignItems: "center",
            flexDirection: "column",
            display: "flex"
          })
        }}
      >
        <div
          className={
            props.isSubmitting
              ? "bg-white w-full max-w-2xl rounded p-8 my-4 mx-auto"
              : "w-full max-w-2xl shadow-md rounded p-8 my-4 mx-auto"
          }
        >
          <div className="flex flex-col justify-center">
            {props.errors.tokenError && (
              <div className="bg-red-300 shadow-md flex flex-col justify-center items-center text-2xl text-center font-bold p-4 mb-3">
                <h1 className="mb-2">
                  Sorry, your payment couldn't be processed.
                </h1>
                {props.errors.tokenError}
              </div>
            )}
            <p className="text-2xl text-center font-bold pb-8">
              Enter your billing information
            </p>
            <div className="flex flex-wrap -mx-3 mb-3">
              <div className="w-full px-3">
                <label className="label" htmlFor="cardHolderName">
                  Name on the card *
                </label>
                <Field
                  name="cardHolderName"
                  type="text"
                  className="input focus:outline-none focus:bg-white focus:border-gray-500"
                />
                <ErrorMessage
                  name="cardHolderName"
                  component="div"
                  className="error"
                />
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-3">
              <div className="w-full px-3">
                <label className="label" htmlFor="cardNumber">
                  Card number
                </label>
                <CardNumberElement
                  style={{ style: elementStyles }}
                  className="input focus:outline-none focus:bg-white focus:border-gray-500"
                />
              </div>
            </div>

            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="label" htmlFor="cardDetails">
                  Expiration date
                </label>
                <CardExpiryElement
                  style={{ style: elementStyles }}
                  className="input focus:outline-none focus:bg-white focus:border-gray-500"
                />
              </div>
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="label" htmlFor="cardDetails">
                  CVC
                </label>
                <CardCvcElement
                  style={{ style: elementStyles }}
                  className="input focus:outline-none focus:bg-white focus:border-gray-500"
                />
              </div>
            </div>

            <button
              type="submit"
              disabled={props.isSubmitting || !props.isValid}
              className="bg-primary-500 hover:bg-primary-700 text-white font-bold py-2 px-4 border border-primary-700 rounded disabled:opacity-50 disabled:cursor-not-allowed"
              onClick={submit}
            >
              {props.isSubmitting && <span>Processing your order...</span>}
              {!props.isSubmitting && <span>Place your order</span>}
            </button>
          </div>
        </div>
      </LoadingOverlay>
    </>
  );
};

export default injectStripe(Checkout);
