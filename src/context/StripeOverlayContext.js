import React from "react";

const StripeOverlayContext = React.createContext();

const StripeOverlayProvider = ({ children }) => {
  const [stripeOverlay, setStripeOverlay] = React.useState(false);

  return (
    <StripeOverlayContext.Provider value={[stripeOverlay, setStripeOverlay]}>
      {children}
    </StripeOverlayContext.Provider>
  );
};

export { StripeOverlayContext, StripeOverlayProvider };
