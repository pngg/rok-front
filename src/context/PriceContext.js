import React from "react";

const PriceContext = React.createContext();

const PriceProvider = ({ children }) => {
  const [totalPrice, setTotalPrice] = React.useState(14900);

  return (
    <PriceContext.Provider value={[totalPrice, setTotalPrice]}>
      {children}
    </PriceContext.Provider>
  );
};

export { PriceContext, PriceProvider };
