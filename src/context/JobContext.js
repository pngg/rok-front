import React from "react";

const JobContext = React.createContext();

const JobProvider = ({ children }) => {
  const [jobData, setJobData] = React.useState({});

  return (
    <JobContext.Provider value={[jobData, setJobData]}>
      {children}
    </JobContext.Provider>
  );
};

export { JobContext, JobProvider };
