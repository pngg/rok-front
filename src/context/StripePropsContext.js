import React from "react";

const StripePropsContext = React.createContext();

const StripePropsProvider = ({ children }) => {
  const [stripeProps, setStripeProps] = React.useState(null);

  return (
    <StripePropsContext.Provider value={[stripeProps, setStripeProps]}>
      {children}
    </StripePropsContext.Provider>
  );
};

export { StripePropsContext, StripePropsProvider };
