import React from "react";

const StandardPackageContext = React.createContext();

const StandardPackageProvider = ({ children }) => {
  const [selectStandard, setSelectStandard] = React.useState(true);

  return (
    <StandardPackageContext.Provider
      value={[selectStandard, setSelectStandard]}
    >
      {children}
    </StandardPackageContext.Provider>
  );
};

export { StandardPackageContext, StandardPackageProvider };
