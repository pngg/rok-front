import React from "react";

const TitleContext = React.createContext();

const TitleProvider = ({ children }) => {
  const [title, setTitle] = React.useState("My Account");

  return (
    <TitleContext.Provider value={[title, setTitle]}>
      {children}
    </TitleContext.Provider>
  );
};

export { TitleContext, TitleProvider };
