import React from "react";
import { StripeProvider } from "react-stripe-elements";

import { UserProvider } from "./UserContext";
import { TitleProvider } from "./TitleContext";
import { JobProvider } from "./JobContext";
import { ThumbProvider } from "./ThumbContext";
import { PriceProvider } from "./PriceContext";
import { StripePropsProvider } from "./StripePropsContext";
import { StripeOverlayProvider } from "./StripeOverlayContext";
import { StandardPackageProvider } from "./StandardPackageContext";
import { FeaturedPackageProvider } from "./FeaturedPackageContext";

import { stripePubKey } from "../helpers"

const Store = ({ children }) => {
  return (
    <UserProvider>
      <TitleProvider>
        <JobProvider>
          <ThumbProvider>
            <StandardPackageProvider>
              <FeaturedPackageProvider>
                <PriceProvider>
                  <StripeOverlayProvider>
                    <StripeProvider apiKey={stripePubKey}>
                      <StripePropsProvider>{children}</StripePropsProvider>
                    </StripeProvider>
                  </StripeOverlayProvider>
                </PriceProvider>
              </FeaturedPackageProvider>
            </StandardPackageProvider>
          </ThumbProvider>
        </JobProvider>
      </TitleProvider>
    </UserProvider>
  );
};

export default Store;
