import React from "react";

const FeaturedPackageContext = React.createContext();

const FeaturedPackageProvider = ({ children }) => {
  const [selectFeatured, setSelectFeatured] = React.useState(false);

  return (
    <FeaturedPackageContext.Provider
      value={[selectFeatured, setSelectFeatured]}
    >
      {children}
    </FeaturedPackageContext.Provider>
  );
};

export { FeaturedPackageContext, FeaturedPackageProvider };
