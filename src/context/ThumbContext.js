import React from "react";

const ThumbContext = React.createContext({});

const ThumbProvider = ({ children }) => {
  const [thumb, setThumb] = React.useState(null);

  return (
    <ThumbContext.Provider value={[thumb, setThumb]}>
      {children}
    </ThumbContext.Provider>
  );
};

export { ThumbContext, ThumbProvider };
