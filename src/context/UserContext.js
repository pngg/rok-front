import React from "react";

const UserContext = React.createContext({});

const UserProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = React.useState(null);
  
  return (
    <UserContext.Provider value={[currentUser, setCurrentUser]}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, UserProvider };
