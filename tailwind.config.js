const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  theme: {
    textStyles: theme => ({
      // defaults to {}
      heading: {
        output: false, // this means there won't be a "heading" component in the CSS, but it can be extended
        fontWeight: theme("fontWeight.bold"),
        lineHeight: theme("lineHeight.tight")
      },
      h1: {
        extends: "heading", // this means all the styles in "heading" will be copied here; "extends" can also be an array to extend multiple text styles
        fontSize: theme("fontSize.5xl"),
        "@screen sm": {
          fontSize: theme("fontSize.6xl")
        }
      },
      h2: {
        extends: "heading",
        fontSize: theme("fontSize.4xl"),
        "@screen sm": {
          fontSize: theme("fontSize.5xl")
        }
      },
      h3: {
        extends: "heading",
        fontSize: theme("fontSize.4xl")
      },
      h4: {
        extends: "heading",
        fontSize: theme("fontSize.3xl")
      },
      h5: {
        extends: "heading",
        fontSize: theme("fontSize.2xl")
      },
      h6: {
        extends: "heading",
        fontSize: theme("fontSize.xl")
      },
      link: {
        fontWeight: theme("fontWeight.bold"),
        color: theme("colors.blue.400"),
        "&:hover": {
          color: theme("colors.blue.600"),
          textDecoration: "underline"
        }
      },
      richText: {
        fontWeight: theme("fontWeight.normal"),
        fontSize: theme("fontSize.base"),
        lineHeight: theme("lineHeight.relaxed"),
        "> * + *": {
          marginTop: "1em"
        },
        h1: {
          extends: "h1"
        },
        h2: {
          extends: "h2"
        },
        h3: {
          extends: "h3"
        },
        h4: {
          extends: "h4"
        },
        h5: {
          extends: "h5"
        },
        h6: {
          extends: "h6"
        },
        ul: {
          listStyleType: "disc"
        },
        ol: {
          listStyleType: "decimal"
        },
        a: {
          extends: "link"
        },
        "b, strong": {
          fontWeight: theme("fontWeight.bold")
        },
        "i, em": {
          fontStyle: "italic"
        }
      }
    }),
    customForms: theme => ({
      default: {
        input: {
          borderRadius: theme("borderRadius.lg"),
          backgroundColor: theme("colors.gray.200"),
          "&:focus": {
            backgroundColor: theme("colors.white")
          }
        },
        select: {
          borderRadius: theme("borderRadius.lg"),
          boxShadow: theme("boxShadow.default")
        },
        checkbox: {
          width: theme("spacing.6"),
          height: theme("spacing.6"),
          iconColor: theme("colors.primary-0")
        }
      }
    }),
    extend: {
      colors: {
        // palette 2
        primary: {
          50: "#003E6B",
          100: "#DCEEFB",
          200: "#B6E0FE",
          300: "#84C5F4",
          400: "#62B0E8",
          500: "#4098D7",
          600: "#2680C2",
          700: "#186FAF",
          800: "#0F609B",
          900: "#0A558C"
        },
        vivid: {
          50: "#8D2B0B",
          100: "#FFFBEA",
          200: "#FFF3C4",
          300: "#FCE588",
          400: "#FADB5F",
          500: "#F7C948",
          600: "#F0B429",
          700: "#DE911D",
          800: "#CB6E17",
          900: "#B44D12"
        },
        neutral: {
          50: "#102A43",
          100: "#F0F4F8",
          200: "#D9E2EC",
          300: "#BCCCDC",
          400: "#9FB3C8",
          500: "#829AB1",
          600: "#627D98",
          700: "#486581",
          800: "#334E68",
          900: "#243B53"
        }
      },
      fontFamily: {
        sans: ["Inter var", ...defaultTheme.fontFamily.sans]
      }
    }
  },
  variants: {
    opacity: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ],
    backgroundColor: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ],
    textColor: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ],
    borderColors: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ],
    visibility: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ],
    cursor: [
      "responsive",
      "group-hover",
      "focus-within",
      "first",
      "last",
      "odd",
      "even",
      "hover",
      "focus",
      "active",
      "visited",
      "disabled"
    ]
  },
  plugins: [
    require("@tailwindcss/ui"),
    require("tailwindcss-typography")({
      // all these options default to the values specified here
      ellipsis: true, // whether to generate ellipsis utilities
      hyphens: true, // whether to generate hyphenation utilities
      kerning: true, // whether to generate kerning utilities
      textUnset: true, // whether to generate utilities to unset text properties
      componentPrefix: "c-" // the prefix to use for text style classes
    })
  ]
};
